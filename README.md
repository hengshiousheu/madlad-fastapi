# Madlad400-FastAPI

Madlad400-FastAPI 提供 Madlad400 系列模型進行翻譯的服務。該項目利用 FastAPI 框架構建，支持通過 REST API 進行快速且高效的文本翻譯。

## 特性

- **快速翻译**：支持多种语言快速翻译，确保翻译质量和速度。
- **自动设备选择**：根据运行环境自动选择最佳设备执行翻译任务（CPU/GPU）。
- **易于部署**：支持 Docker 容器化部署，简化部署流程。

## 快速开始

### 环境要求

- Python 3.8 或更高版本
- FastAPI
- Uvicorn 用于运行服务
- Docker（可选，用于容器化部署）

### 安装步骤

### 配置与环境变量
通过环境变量进行配置，支持以下环境变量：

- MADLAD400_MODEL_NAME: 使用的模型名称，默认为 "Heng666/madlad400-3b-mt-ct2-int8"。
- DEVICE: 指定运行设备，"auto"、"cpu" 或 "cuda"。
- CT2_USE_EXPERIMENTAL_PACKED_GEMM: 是否使用实验性的 GEMM 打包，默认为 "1"。

## DeepL Glossary 管理

### 設置 Glossary

1. 準備 CSV 文件：
   創建一個 CSV 文件，包含多語言翻譯。CSV 的標題行應該遵循以下格式：
   ```
   中文-ZH,日文-JA,英文-EN,韓文-KO,泰文-TH,越南文-VI,印尼文-ID,馬來西亞文-MS,西班牙文-ES,義大利文-IT,德文-DE,荷蘭文-NL,葡萄牙文-PT,法文-FR,波蘭文-PL,俄羅斯文-RU,土耳其文-TR,瑞典文-SV
   ```

2. 配置 YAML 文件：
   創建或編輯 `glossaries.yaml` 文件，定義詞彙表配置：

   ```yaml
   glossaries:
     - name: "多語言旅遊詞彙表"
       csv_file: "翻譯result_系統用.csv"
       domain: "travel"
       language_pairs:
         ZH: [JA, EN, KO, TH, VI, ID, MS, ES, IT, DE, NL, PT, FR, PL, RU, TR, SV]
         JA: [ZH]
         EN: [ZH]
         KO: [ZH]
         TH: [ZH]
         VI: [ZH]
         ID: [ZH]
         MS: [ZH]
         ES: [ZH]
         IT: [ZH]
         DE: [ZH]
         NL: [ZH]
         PT: [ZH]
         FR: [ZH]
         PL: [ZH]
         RU: [ZH]
         TR: [ZH]
         SV: [ZH]
   ```

3. 運行 Glossary 管理腳本：
   ```bash
   python3 app/services/manage_deepl_glossaries.py --config glossaries.yaml
   ```

### 使用 Glossary API

- 列出所有詞彙表：
  ```
  GET /list_glossaries/
  ```

- 刪除特定詞彙表：
  ```
  DELETE /delete_glossary/{glossary_id}
  ```

- 刪除所有詞彙表：
  ```
  DELETE /delete_all_glossaries
  ```

### 本地開發
```bash
./run_local.sh
```

### 測試指令說明

本專案使用 `pytest` 進行單元測試和覆蓋率報告。以下是執行測試的具體指令及其參數說明。

#### 測試指令

```bash
pytest -q --tb=short --disable-warnings --cov=app --cov-report=html
```

#### 指令參數說明

- `-q`:
  - **簡化輸出** (quiet mode)
  - 減少測試過程中的輸出訊息，只顯示關鍵結果。
  
- `--tb=short`:
  - **簡短錯誤追蹤** (short traceback)
  - 當測試失敗時，只顯示簡短的錯誤追蹤資訊，方便快速定位問題。

- `--disable-warnings`:
  - **禁用警告** (disable warnings)
  - 測試過程中不顯示警告訊息，讓輸出更加清晰。

- `--cov=app`:
  - **覆蓋率報告** (coverage report)
  - 指定需要進行覆蓋率分析的目錄或模組，此處為 `app`。
  - 此參數會生成測試覆蓋率數據，幫助了解代碼中哪些部分被測試覆蓋。

- `--cov-report=html`:
  - **HTML 覆蓋率報告** (HTML coverage report)
  - 生成 HTML 格式的測試覆蓋率報告，便於在瀏覽器中查看詳細的覆蓋率資訊。

#### 生成的覆蓋率報告

運行指令後，會在專案目錄中生成一個 `htmlcov` 目錄，包含 HTML 格式的覆蓋率報告。打開 `htmlcov/index.html` 文件即可在瀏覽器中查看詳細的覆蓋率資訊。


### 本地開發（Docker 封裝）
```bash
docker build --load --platform linux/amd64 -t xiuxiumycena/madlad-fastapi_dev:0.0.3 .
docker push xiuxiumycena/madlad-fastapi_dev:0.0.3

docker build --load --platform linux/amd64 -f Dockerfile.gpu -t xiuxiumycena/madlad-fastapi_dev:gpu-0.0.3 .
docker push xiuxiumycena/madlad-fastapi_dev:gpu-0.0.3
```

開發階段可以使用，
backend 會直接 build . 來處理
```bash
docker-compose -f docker-compose-dev.yml up 
```

生產階段可以使用
backend 會採用 docker hug 上面 image 來處理
```bash
docker-compose -f docker-compose-prod.yml up 
```

Docker GPU 可以使用
```bash
docker-compose -f docker-compose.gpu.yml up 
```

Tilt 協助 k8s 開發
```
確認本機已經跟開發用 k8s 環境連接
kubectl config get-contexts

接著就可以下
tilt up 
```