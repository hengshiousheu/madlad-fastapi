from app.strategy.enum import TranslationStrategyEnum
from app.strategy.strategies import TranslationStrategyFactory
from fastapi import APIRouter, HTTPException, Depends, Query
from typing import Optional
from app.schema.translation import BatchTranslationRequest, BatchTranslationResponse
from app.services.batch_translatoin_service import BatchTranslationService, get_batch_translation_service
from loguru import logger
from opencc import OpenCC
from app.services.ac_term_service import ACTermService
import time

router = APIRouter()
cc = OpenCC('s2twp')  # convert from Simplified Chinese to Traditional Chinese
term_service = ACTermService()  # 創建術語服務實例
strategy_factory: TranslationStrategyFactory = TranslationStrategyFactory()
translation_service: BatchTranslationService = BatchTranslationService(strategy_factory = strategy_factory)

@router.post("/batch-translate/", response_model=BatchTranslationResponse)
async def translate_multiple_languages(
    request: BatchTranslationRequest,
    strategy: Optional[TranslationStrategyEnum] = Query(TranslationStrategyEnum.MADLAD400, description="The translation strategy to use. Options are: default, deepl, google, madlad400, bridging. Default is Madlad strategy."),
    # translation_service: BatchTranslationService = Depends(get_batch_translation_service)
):
    """
    多語言翻譯 API 端點。

    :param request: MultiLanguageTranslationRequest, 包含要翻譯的文本和目標語言列表
    :param translation_service: BatchTranslationService, 注入的批量翻譯服務
    :return: MultiLanguageTranslationResponse, 每種語言的翻譯結果
    """
    start_time = time.time()

    try:

        translated_sentence = await translation_service.translate_multiple_languages(
            request.input_text,
            request.target_languages,
            request.source_language,
            strategy
        )

        translation_result = {
            "source_sentence": request.input_text,
            "source_language":  request.source_language if request.source_language else "Non_provided",
            "target_languages": request.target_languages,
            "target_sentences": translated_sentence,
            "translated_text": translated_sentence
        }

        if "zh" in translation_result['source_language']:
            translation_result['source_language'] = "zh_Hant"
            translation_result['source_sentence'] = cc.convert(translation_result['source_sentence'])  #繁體化
            translation_result['source_sentence'] = term_service.convert_text(translation_result['source_sentence']) # 台灣化
        
        if 'zh' in translation_result['target_sentences']:
            zh_text = translation_result['target_sentences']['zh']
            # 先繁體化
            zh_text = cc.convert(zh_text)
            # 再台灣化
            zh_text = term_service.convert_text(zh_text)
            translation_result['target_sentences']['zh'] = zh_text
            translation_result['translated_text']['zh'] = zh_text
        
        execution_time = round(time.time() - start_time, 6)
        translation_result["execution_time"] = execution_time

        return BatchTranslationResponse(**translation_result)

    except Exception as e:
        logger.error(f"多語言翻譯失敗: {e}")
        raise HTTPException(status_code=500, detail=f"多語言翻譯失敗: {e}")