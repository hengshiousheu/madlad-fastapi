from fastapi import APIRouter

router = APIRouter()

@router.get("/languages/")
async def get_supported_languages():
    """
    获取翻译支持的语言列表。
    """
    return {
        "languages": [
            {"code": "en", "name": "英语"},
            {"code": "zh", "name": "简体中文"},
            {"code": "zh_Hant", "name": "繁体中文"},
            {"code": "ja", "name": "日语"},
            {"code": "ko", "name": "韩语"},
            {"code": "vi", "name": "越南语"},
            {"code": "id", "name": "印尼语"},
            {"code": "th", "name": "泰语"},
            {"code": "ms", "name": "马来西亚语"},
            {"code": "es", "name": "西班牙语"},
            {"code": "it", "name": "意大利语"},
            {"code": "de", "name": "德语"},
            {"code": "nl", "name": "荷兰语"},
            {"code": "pt", "name": "葡萄牙语"},
            {"code": "fr", "name": "法语"},
            {"code": "pl", "name": "波兰语"},
            {"code": "ru", "name": "俄语"},
            {"code": "tr", "name": "土耳其语"},
            {"code": "sv", "name": "瑞典语"},
        ]
    }
