from fastapi import APIRouter, HTTPException, Depends
from app.schema.translation import TranslationRequest, TranslationResponse
from app.services.madlad400_translator import Madlad400TranslatorService
from loguru import logger
import time

router = APIRouter()

def get_translate_service():
    return Madlad400TranslatorService()

@router.post("/translate_madlad400/", response_model=TranslationResponse)
async def translate(
    request: TranslationRequest,
    translate_service: Madlad400TranslatorService = Depends(get_translate_service)
):
    """
    將文本從任何支持的語言翻譯成另一種語言。

    - **input_text**: 想要翻譯的文本。
    - **target_language**: 你想將文本翻譯成的目標語言的 ISO 代碼。
    """
    start_time = time.time()
    try:
        translated_sentence = translate_service.translate_text(
            request.input_text, 
            request.target_language, 
            request.source_language
        )
    except ValueError as e:
        raise HTTPException(status_code=400, detail=str(e))
    except RuntimeError as e:
        raise HTTPException(status_code=500, detail=str(e))
    
    execution_time = round(time.time() - start_time, 6)

    return TranslationResponse(
        translated_text=translated_sentence,
        execution_time=execution_time
    )
