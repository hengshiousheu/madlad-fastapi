from typing import Optional
from app.strategy.enum import TranslationStrategyEnum
from fastapi import APIRouter, HTTPException, Depends, Query
from app.schema.zero_shot_query import ZeroShotQueryTranslationRequest, ZeroShotQueryTranslationResponse
from app.strategy.strategies import TranslationStrategy, TranslationStrategyFactory
from opencc import OpenCC
import os
from loguru import logger
from app.core.config import settings
from app.schema.zero_shot_query import Language
from app.services.ac_term_service import ACTermService
import time

cc = OpenCC('s2twp')  # convert from Simplified Chinese to Traditional Chinese
term_service = ACTermService()  # 創建術語服務實例

zero_shot_query_translation_router = APIRouter()
strategy_factory: TranslationStrategyFactory = TranslationStrategyFactory()

def validate_language(language: str):
    """
    驗證是否為支持的語言。
    """
    if language not in Language.__members__.values():
        raise ValueError(f'{language} is not a supported language')
    return language

def truncate_text(text: str) -> str:
    """
    對輸入文本進行預處理，包括移除句子結尾的指定字串。
    """
    symbols = os.getenv("TRUNCATION_SYMBOLS", "，。-|>><<").split("|")
    for symbol in symbols:
        index = text.find(symbol)
        if index != -1:
            return text[:index]
    return text

@zero_shot_query_translation_router.post(
        "/zero-shot-query", 
        description="Translate text from one language to another. Supported languages are: Traditional Chinese (zh), English (en), Japanese (ja), Korean (ko), Indonesian (id), Vietnamese (vi), Thai (th).", 
        response_model=ZeroShotQueryTranslationResponse
        )
async def zero_shot_query(
    request: ZeroShotQueryTranslationRequest,
    strategy_name: Optional[TranslationStrategyEnum] = Query(None, description="The translation strategy to use. Options are: deepl, google, madlad400, bridging. Default is google.")
    ):
    """
    將文本從任何支持的語言翻譯成另一種語言。

    - **input_text**: 想要翻譯的文本。
    - **target_language**: 你想將文本翻譯成的目標語言的 ISO 代碼。
    - **strategy_name**: 使用的翻譯策略名稱，可選值：deepl, google, madlad400, bridging, default。
    """
    start_time = time.time()
    try:

        # 基本參數處理
        source_sentence = request.query.strip()
        request_source_language= request.source_language
        request_target_language = request.target_language

        # 驗證語言支援
        validate_language(request_source_language)
        validate_language(request_target_language)

        # 決定翻譯策略
        ## 判斷 strategy_name，如果 Query 中沒有提供，則使用 settings 中的值
        if strategy_name is None:
            strategy_name = settings.MACHINE_TRANSLATION_STRATEGY
        ## 如果 settings 中的值不屬於 enum，則使用 DEFAULT
        if strategy_name not in TranslationStrategyEnum._value2member_map_:
            strategy_name = TranslationStrategyEnum.DEFAULT

        # 創建翻譯策略
        strategy: TranslationStrategy = strategy_factory.create_strategy(strategy_name)
        translated_sentence = strategy.translate(source_sentence, request_target_language, request_source_language)
        logger.info(f'使用 {strategy.__class__.__name__ or "default"} 進行翻譯: {source_sentence} -> {translated_sentence}')

        translated_sentence = truncate_text(translated_sentence)

        if "zh" in request_source_language:
            request_source_language = "zh_Hant"
            source_sentence = cc.convert(source_sentence)
            logger.debug(f'轉換原始句子 {source_sentence}')
        if "zh" in request_target_language:
            request_target_language = "zh_Hant"
            translated_sentence = cc.convert(translated_sentence) #繁體化
            translated_sentence = term_service.convert_text(translated_sentence) # 台灣化
            logger.debug(f'轉換翻譯句子 {translated_sentence}')
        
        execution_time = round(time.time() - start_time, 6)

    except ValueError as e:
        raise HTTPException(status_code=400, detail=f'Error: {str(e)}')
    return ZeroShotQueryTranslationResponse(
            source_sentence=source_sentence,
            source_language=request_source_language,
            target_language=request_target_language,
            target_sentence=translated_sentence,
            execution_time=execution_time
        )
