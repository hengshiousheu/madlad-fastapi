from fastapi import APIRouter, HTTPException, Depends
from app.schema.translation import TranslationRequest, TranslationResponse
from app.services.bridging_translator import BridgingTranslatorService, get_bridging_translator_service
import time
router = APIRouter()

# 可以使用依赖注入获取翻译服务实例
def get_translate_service():
    return BridgingTranslatorService()

@router.post("/bridge_translate/", response_model=TranslationResponse)
async def bridge_translate(
    request: TranslationRequest,
    bridging_translator_service: BridgingTranslatorService = Depends(get_bridging_translator_service)
):
    """
    將文本從任何支持的語言先翻譯成中間語言，再翻譯成目標語言。

    - **input_text**: 想要翻譯的文本。
    - **target_language**: 你想將文本翻譯成的目標語言的 ISO 代碼。
    """
    start_time = time.time()
    try:
        translated_sentence = bridging_translator_service.bridge_translate(
            request.input_text, 
            request.target_language, 
            request.source_language
        )
    except ValueError as e:
        raise HTTPException(status_code=400, detail=str(e))
    except RuntimeError as e:
        raise HTTPException(status_code=500, detail=str(e))
    
    execution_time = round(time.time() - start_time, 6)

    return TranslationResponse(
        translated_text=translated_sentence,
        execution_time=execution_time
    )
