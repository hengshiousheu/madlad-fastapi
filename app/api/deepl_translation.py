# app/api/router.py

from fastapi import APIRouter, HTTPException, Depends
from fastapi.responses import JSONResponse
from app.schema.translation import TranslationRequest, TranslationResponse
from app.services.deepl_translator import DeepLTranslatorService
from app.core.config import settings
from loguru import logger
import sys
from typing import List, Dict
import deepl
import time

# 使用範例
# 1. 啟動 FastAPI 應用程式
# 2. 使用 Postman 或其他 API 測試工具發送 POST 請求到 /translate_deepl/
# 3. 請求的 JSON 格式如下：
# {
#     "input_text": "Hello, world!",
#     "target_language": "FR"
# }
# 4. 期望的回應結果：
# {
#     "translated_text": "Bonjour, le monde !"
# }

router = APIRouter()

# 可以使用依賴注入獲取翻譯服務實例
def get_translate_service():
    return DeepLTranslatorService(auth_key=settings.DEEPL_AUTH_KEY_JSON["key"])

@router.post("/translate_deepl/", response_model=TranslationResponse)
async def translate(
        request: TranslationRequest, 
        translate_service: DeepLTranslatorService = Depends(get_translate_service)
    ):
    """
    將文本從任何支持的語言翻譯成另一種語言。
    """
    start_time = time.time()
    try:
        logger.info(f"將文本 '{request.input_text}' 翻譯成目標語言 '{request.target_language}'")
        translated_sentence = translate_service.translate(request.input_text, request.target_language, request.source_language)
    except ValueError as e:
        logger.warning(f"翻譯請求錯誤: {str(e)}")
        raise HTTPException(status_code=400, detail=str(e))
    except deepl.DeepLException as e:
        logger.error(f"DeepL 翻譯過程中發生錯誤: {str(e)}")
        raise HTTPException(status_code=500, detail=f"DeepL 翻譯過程中發生錯誤: {str(e)}")
    except Exception as e:
        logger.error(f"翻譯過程中發生未知錯誤: {str(e)}")
        raise HTTPException(status_code=500, detail=f"翻譯過程中發生未知錯誤: {str(e)}")
    
    execution_time = round(time.time() - start_time, 6)

    return TranslationResponse(
        translated_text=translated_sentence,
        execution_time=execution_time
    )

@router.get("/available_languages/")
async def get_available_languages(translate_service: DeepLTranslatorService = Depends(get_translate_service)):
    """
    獲取當前可用的源語言和目標語言。
    """
    try:
        languages = translate_service.get_languages()
        return languages
    except Exception as e:
        logger.error(f"獲取語言失敗: {str(e)}")
        raise HTTPException(status_code=500, detail=f"獲取語言失敗: {str(e)}")

@router.get("/list_glossaries/", response_model=List[Dict])
async def list_glossaries(
    translate_service: DeepLTranslatorService = Depends(get_translate_service)
):
    """
    列出所有可用的詞彙表。

    返回:
    List[Dict]: 包含詞彙表信息的字典列表。每個字典包含以下鍵：
    - glossary_id: 詞彙表的唯一標識符
    - name: 詞彙表名稱
    - source_lang: 源語言代碼
    - target_lang: 目標語言代碼
    - entry_count: 詞彙表中的條目數量
    """
    try:
        glossaries = translate_service.list_glossaries()
        return [
            {
                "glossary_id": g.glossary_id,
                "name": g.name,
                "source_lang": g.source_lang,
                "target_lang": g.target_lang,
                "entry_count": g.entry_count
            }
            for g in glossaries
        ]
    except Exception as e:
        raise HTTPException(status_code=500, detail=f"獲取詞彙表列表失敗: {str(e)}")

@router.delete("/delete_glossary/{glossary_id}")
async def delete_glossary(
    glossary_id: str,
    translate_service: DeepLTranslatorService = Depends(get_translate_service)
):
    """
    刪除指定 ID 的詞彙表。

    此端點允許用戶通過提供詞彙表的唯一標識符（glossary_id）來刪除特定的詞彙表。
    操作成功後，將返回一個確認消息。如果在刪除過程中遇到任何錯誤，將拋出 HTTP 異常。

    參數:
    - glossary_id (str): 要刪除的詞彙表的唯一標識符。這是一個路徑參數，
                         直接從 URL 中提取。
    - translate_service (DeepLTranslatorService): DeepL 翻譯服務的實例。
                                                  這個參數通過依賴注入提供，
                                                  使用 get_translate_service 函數。

    返回:
    - dict: 包含確認消息的字典。
            格式: {"message": "Glossary {glossary_id} has been deleted"}

    異常:
    - HTTPException (400): 如果刪除過程中發生錯誤，將拋出狀態碼為 400 的 HTTP 異常。
                           異常的詳細信息將包含錯誤描述。

    注意:
    - 此操作不可逆。一旦詞彙表被刪除，將無法恢復。
    - 在調用此端點之前，請確保您確實想要刪除該詞彙表。
    - 建議在執行刪除操作之前，先使用 list_glossaries 端點檢查詞彙表的詳細信息。
    """
    try:
        # 調用翻譯服務的 delete_glossary 方法來刪除指定的詞彙表
        translate_service.delete_glossary(glossary_id)
        # 如果刪除成功，返回確認消息
        return {"message": f"Glossary {glossary_id} has been deleted"}
    except Exception as e:
        # 如果在刪除過程中遇到任何錯誤，拋出 HTTP 異常
        # 這將自動被 FastAPI 捕獲並轉換為適當的 HTTP 響應
        raise HTTPException(status_code=400, detail=str(e))

@router.delete("/delete_all_glossaries")
async def delete_all_glossaries(
    translate_service: DeepLTranslatorService = Depends(get_translate_service)
):
    """
    刪除所有現有的詞彙表。

    此端點會獲取所有詞彙表並逐一刪除它們。操作完成後，將返回刪除的詞彙表數量。
    如果在刪除過程中遇到任何錯誤，將返回已成功刪除的詞彙表數量以及錯誤詳情。

    返回:
    - dict: 包含確認消息和刪除的詞彙表數量。

    異常:
    - HTTPException (400): 如果在獲取或刪除詞彙表過程中發生錯誤。
    """
    try:
        # 獲取所有詞彙表
        glossaries: List = translate_service.list_glossaries()
        deleted_count = 0
        errors = []

        # 逐一刪除詞彙表
        for glossary in glossaries:
            try:
                translate_service.delete_glossary(glossary.glossary_id)
                deleted_count += 1
            except Exception as e:
                errors.append(f"Failed to delete glossary {glossary.glossary_id}: {str(e)}")

        # 準備響應消息
        message = "All glossaries have been deleted" if not errors else "Some glossaries could not be deleted"
        response_content = {
            "message": message,
            "deleted_count": deleted_count
        }

        if errors:
            response_content["errors"] = errors

        return JSONResponse(
            status_code=200,
            content=response_content
        )

    except Exception as e:
        raise HTTPException(status_code=400, detail=f"Error occurred while deleting glossaries: {str(e)}")