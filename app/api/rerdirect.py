from fastapi import APIRouter, HTTPException
from fastapi.responses import JSONResponse, RedirectResponse
from app.core.config import settings

redirect_router = APIRouter()

@redirect_router.get("/", response_class=RedirectResponse, include_in_schema=False)
async def root():

    return "/docs"
