# app/api/router.py

from fastapi import APIRouter, HTTPException, Depends
from app.schema.translation import TranslationRequest, TranslationResponse
from app.services.google_translator import GoogleTranslateService
from app.core.config import settings
from loguru import logger
import time

router = APIRouter()

# 可以使用依赖注入获取翻译服务实例
def get_translate_service():
    return GoogleTranslateService(project_id=settings.GOOGLE_APPLICATION_CREDENTIALS_JSON["project_id"])

@router.post("/translate_google/", response_model=TranslationResponse)
async def translate(
        request: TranslationRequest, 
        translate_service: GoogleTranslateService = Depends(get_translate_service)
    ):
    """
    将文本从任何支持的语言翻译成另一种语言。
    """
    start_time = time.time()
    try:
        logger.info(f"Translating text from '{request.input_text}' to '{request.target_language}'")
        translated_sentence = translate_service.translate(request.input_text, request.target_language, request.source_language)
    except ValueError as e:
        logger.warning(f"Translation request error: {str(e)}")
        raise HTTPException(status_code=400, detail=str(e))
    except Exception as e:
        logger.error(f"An error occurred during translation: {str(e)}")
        raise HTTPException(status_code=500, detail=f"An error occurred during translation: {str(e)}")
    
    execution_time = round(time.time() - start_time, 6)

    return TranslationResponse(
        translated_text=translated_sentence,
        execution_time=execution_time
    )
