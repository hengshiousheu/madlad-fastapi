from fastapi import APIRouter
from app.api.madlad400_translation import router as madlad400_translation_router
from app.api.bridging_translation import router as bridging_translation_router
from app.api.support_languages import router as support_languages_router
from app.api.usage_model import router as usage_model_router
from app.api.metainfo import root_router  # 如果欢迎消息路由在另一个文件中定义
from app.api.zero_shot_query import zero_shot_query_translation_router
from app.api.google_translation import router as google_translation_router
from app.api.deepl_translation import router as deepl_translation_router
from app.api.batch_translation import router as batch_translation_router
from app.api.rerdirect import redirect_router

api_router = APIRouter()
api_router.include_router(root_router)  # 添加欢迎消息路由，没有前缀
api_router.include_router(redirect_router)
api_router.include_router(usage_model_router, prefix="/v1", tags=["config"])
api_router.include_router(support_languages_router, prefix="/v1", tags=["config"])

api_router.include_router(zero_shot_query_translation_router, prefix="/api/translation", tags=["translation"])
api_router.include_router(madlad400_translation_router, prefix="/v1", tags=["translation"])

api_router.include_router(bridging_translation_router, prefix="/v1", tags=["pretranslation"])
api_router.include_router(google_translation_router, prefix="/api/google_translation", tags=["external_translation"])
api_router.include_router(deepl_translation_router, prefix="/api/deepl_translation", tags=["external_translation"])

api_router.include_router(batch_translation_router, prefix="/v1", tags=["batch"])