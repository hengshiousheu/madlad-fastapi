
from fastapi import APIRouter, HTTPException
from app.core.config import settings
router = APIRouter()

@router.get("/usage_model")
async def usage_model():
    """
    目前環境中使用的模型版本，應該能夠在 HuggingFace 當中查詢到
    """
    return {"hf_model_name": settings.MADLAD400_MODEL_NAME}
