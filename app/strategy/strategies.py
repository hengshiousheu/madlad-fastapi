from abc import ABC, abstractmethod
from app.strategy.enum import TranslationStrategyEnum
from fastapi import Depends
from app.services.deepl_translator import DeepLTranslatorService
from app.services.google_translator import GoogleTranslateService
from app.services.madlad400_translator import Madlad400TranslatorService
from app.services.bridging_translator import BridgingTranslatorService
from app.services.dependency_injection import get_deepl_translator_service, get_google_translator_service, get_madlad400_translator_service, get_bridging_translator_service
from loguru import logger
from typing import Optional

class TranslationStrategy(ABC):
    """
    翻譯策略的抽象基類，定義了翻譯方法。
    所有具體的翻譯策略必須實現此類。
    """
    @abstractmethod
    def translate(self, text: str, target_language: str, source_language: str = None) -> str:
        pass

class DeepLStrategy(TranslationStrategy):
    """
    使用 DeepL 翻譯服務的具體策略，失敗時回退到 Madlad400Service。
    """
    def __init__(self, deepl_service: DeepLTranslatorService, madlad400_service: Madlad400TranslatorService):
        self.deepl_service = deepl_service
        self.madlad400_service = madlad400_service

    def translate(self, text: str, target_language: str, source_language: str) -> str:
        try:
            return self.deepl_service.translate(text, target_language, source_language)
        except Exception as e:
            logger.warning(f"DeepL 翻譯失敗，嘗試使用 Madlad400Service：{e}")
            return self.madlad400_service.translate_text(text, target_language, source_language)

class GoogleStrategy(TranslationStrategy):
    """
    使用 Google 翻譯服務的具體策略，失敗時回退到 Madlad400Service。
    """
    def __init__(self, google_service: GoogleTranslateService, madlad400_service: Madlad400TranslatorService):
        self.google_service = google_service
        self.madlad400_service = madlad400_service

    def translate(self, text: str, target_language: str, source_language: str) -> str:
        try:
            return self.google_service.translate(text, target_language, source_language)
        except Exception as e:
            logger.warning(f"Google 翻譯失敗，嘗試使用 Madlad400Service：{e}")
            return self.madlad400_service.translate_text(text, target_language, source_language)

class Madlad400Strategy(TranslationStrategy):
    """
    使用自研模型翻譯服務的具體策略。
    """
    def __init__(self, service: Madlad400TranslatorService):
        self.service = service

    def translate(self, text: str, target_language: str, source_language: str) -> str:
        return self.service.translate_text(text, target_language, source_language)

class BridgingStrategy(TranslationStrategy):
    """
    使用中間語言進行橋接翻譯的具體策略，失敗時回退到 Madlad400Service。
    """
    def __init__(self, service: BridgingTranslatorService, madlad400_service: Madlad400TranslatorService):
        self.service = service
        self.madlad400_service = madlad400_service

    def translate(self, text: str, target_language: str, source_language: str) -> str:
        try:
            return self.service.bridge_translate(text, target_language)
        except Exception as e:
            logger.warning(f"Bridging 翻譯失敗，嘗試使用 Madlad400Service：{e}")
            return self.madlad400_service.translate_text(text, target_language, source_language)

class DefaultStrategy(TranslationStrategy):
    """
    默認的翻譯策略，按順序嘗試不同的翻譯服務：
    先使用 DeepL，失敗則使用 Google，再失敗則使用 Madlad400Service。
    """
    def __init__(self, 
                 deepl_service: DeepLTranslatorService, 
                 google_service: GoogleTranslateService, 
                 madlad400_service: Madlad400TranslatorService):
        self.deepl_service = deepl_service
        self.google_service = google_service
        self.madlad400_service = madlad400_service

    def translate(self, text: str, target_language: str, source_language: str) -> str:
        try:
            return self.deepl_service.translate(text, target_language, source_language)
        except Exception as e:
            logger.warning(f"DeepL 翻譯失敗：{e}")
            try:
                return self.google_service.translate(text, target_language, source_language)
            except Exception as e:
                logger.warning(f"Google 翻譯失敗：{e}")
                return self.madlad400_service.translate_text(text, target_language, source_language)


class TranslationStrategyFactory:
    _instance = None

    def __new__(cls, *args, **kwargs):
        if cls._instance is None:
            cls._instance = super().__new__(cls)
        return cls._instance

    def __init__(self, 
                deepl_service=get_deepl_translator_service(),
                google_service=get_google_translator_service(),
                madlad400_service=get_madlad400_translator_service(),
                bridging_service=get_bridging_translator_service()):
        self.deepl_service = deepl_service
        self.google_service = google_service
        self.madlad400_service = madlad400_service
        self.bridging_service = bridging_service

    def create_strategy(self, strategy_name: Optional[TranslationStrategyEnum]) -> TranslationStrategy:
        """
        根據策略名稱獲取對應的翻譯策略實例。
        """
        if strategy_name == TranslationStrategyEnum.DEEPL:
            return DeepLStrategy(self.deepl_service, self.madlad400_service)
        elif strategy_name == TranslationStrategyEnum.GOOGLE:
            return GoogleStrategy(self.google_service, self.madlad400_service)
        elif strategy_name == TranslationStrategyEnum.MADLAD400:
            return Madlad400Strategy(self.madlad400_service)
        elif strategy_name == TranslationStrategyEnum.BRIDGING:
            return BridgingStrategy(self.bridging_service, self.madlad400_service)
        else:
            return DefaultStrategy(self.deepl_service, self.google_service, self.madlad400_service)
