from enum import Enum

class TranslationStrategyEnum(str, Enum):
    DEEPL = "deepl"
    GOOGLE = "google"
    MADLAD400 = "madlad400"
    BRIDGING = "bridging"
    DEFAULT = "default"