import time
from dataclasses import dataclass
from enum import Enum
import ahocorasick
from typing import Dict, List
from app.schema.ac_term import Term, TermType

class ACTermService:
    """AC 自動機術語轉換服務"""
    
    def __init__(self):
        """初始化服務"""
        self.terms: Dict[str, Term] = {}
        self.automaton = ahocorasick.Automaton()
        self.version = 0
        self.last_updated = time.time()
        self._initialize_terms()
        
    def _initialize_terms(self) -> None:
        """
        初始化預設術語

        分類說明：
        GENERAL: 生活常用語
        TECHNICAL: 科技術語
        INTERNET: 網路用語
        PLACE: 地名用語
        FOOD: 飲食用語
        TRAVEL: 旅遊用語
        BUSINESS: 商業用語
        TPE: 桃園機場專用
        """
        default_terms = [
            # 生活常用語 (GENERAL)
            Term("早上好", "早安", TermType.GENERAL),
            Term("晚上好", "晚安", TermType.GENERAL),
            Term("下午好", "午安", TermType.GENERAL),
            Term("再見", "拜拜", TermType.GENERAL),
            Term("小區", "社區", TermType.GENERAL),
            Term("公交", "公車", TermType.GENERAL),
            Term("地鐵", "捷運", TermType.GENERAL),
            Term("自行車", "腳踏車", TermType.GENERAL),
            Term("信息", "資訊", TermType.GENERAL),
            Term("垃圾桶", "垃圾筒", TermType.GENERAL),
            Term("衛生間", "廁所", TermType.GENERAL),
            Term("小區", "社區", TermType.GENERAL),
            Term("商場", "賣場", TermType.GENERAL),
            Term("寫字樓", "辦公大樓", TermType.GENERAL),
            Term("樓層", "樓層", TermType.GENERAL),
            Term("電梯", "電梯", TermType.GENERAL),
            Term("陽台", "陽臺", TermType.GENERAL),
            Term("景區", "景點", TermType.TRAVEL),
            Term("景點", "觀光景點", TermType.TRAVEL),
            Term("古蹟", "古蹟", TermType.TRAVEL),
            Term("步行街", "徒步區", TermType.TRAVEL),
            Term("遊樂場", "遊樂園", TermType.TRAVEL),
            Term("海水浴場", "海水浴場", TermType.TRAVEL),
            Term("溫泉", "溫泉", TermType.TRAVEL),

            # 生活常用語 (DAILY)
            Term("請稍等", "請稍候", TermType.DAILY),
            Term("傻逼", "傻蛋", TermType.DAILY),
            Term("牛逼", "厲害", TermType.DAILY),
            Term("紅火", "火紅", TermType.DAILY),
            Term("地道", "道地", TermType.DAILY),
            Term("碰瓷", "假摔", TermType.DAILY),
            Term("視頻", "影片", TermType.DAILY),
            Term("視頻通話", "視訊通話", TermType.DAILY),
            Term("信息", "訊息", TermType.DAILY),
            Term("叫板", "嗆聲", TermType.DAILY),
            Term("很不靠譜", "很不可靠", TermType.DAILY),
            Term("很到位", "很完備", TermType.DAILY),
            Term("服務員", "服務生", TermType.DAILY),
            Term("夜宵", "宵夜", TermType.DAILY),
            Term("衛生巾", "衛生棉", TermType.DAILY),
            Term("沖涼", "洗澡", TermType.DAILY),
            Term("貓膩", "隱情", TermType.DAILY),
            Term("NMSL", "幹你娘", TermType.DAILY),
            Term("黃油", "奶油", TermType.DAILY),
            Term("有一說一", "平心而論", TermType.DAILY),
            Term("小姊姊", "正妹", TermType.DAILY),
            Term("AWSL", "幹我要死了", TermType.DAILY),
            Term("見不得人", "見不得人", TermType.DAILY),
            Term("也是醉了", "沒救了", TermType.DAILY),
            Term("實打實", "穩紮穩打", TermType.DAILY),
            Term("樂子人", "唯恐天下不亂", TermType.DAILY),
            Term("破防", "深受打擊", TermType.DAILY),
            Term("社牛", "熱情", TermType.DAILY),
            Term("臥槽", "哇靠", TermType.DAILY),
            Term("辣眼睛", "慘不忍睹", TermType.DAILY),
            Term("質量", "品質", TermType.DAILY),
            Term("快進", "快轉", TermType.DAILY),

            # 旅遊用語 (TRAVEL)
            Term("航空站", "機場", TermType.TRAVEL),
            Term("航空樓", "航廈", TermType.TRAVEL),
            Term("地鐵站", "捷運站", TermType.TRAVEL),
            Term("出租車", "計程車", TermType.TRAVEL),
            Term("輪渡", "渡輪", TermType.TRAVEL),
            Term("公交車", "公車", TermType.TRAVEL),
            Term("公交車站", "公車站", TermType.TRAVEL),
            Term("動車", "高鐵", TermType.TRAVEL),
            Term("景區", "景點", TermType.TRAVEL),
            Term("地圖", "地圖", TermType.TRAVEL),
            Term("地鐵", "捷運", TermType.TRAVEL),
            Term("高鐵", "高鐵", TermType.TRAVEL),
            Term("出租車", "計程車", TermType.TRAVEL),
            Term("單車", "腳踏車", TermType.TRAVEL),
            Term("共享單車", "共享腳踏車", TermType.TRAVEL),
            Term("立交橋", "天橋", TermType.TRAVEL),
            Term("迪士", "計程車", TermType.TRAVEL),
            Term("打迪", "搭計程車", TermType.TRAVEL),
            Term("打的", "搭計程車", TermType.TRAVEL),
            Term("打車", "搭計程車", TermType.TRAVEL),
            Term("方便麵", "泡麵", TermType.TRAVEL),
            Term("新西蘭", "紐西蘭", TermType.TRAVEL),
            Term("意大利", "義大利", TermType.TRAVEL),
            Term("特斯拉", "Tesla", TermType.TRAVEL),  # 增加更常見品牌
            Term("自助遊", "自由行", TermType.TRAVEL),
            Term("全家桶", "套餐", TermType.TRAVEL),
            Term("充電寶", "行動電源", TermType.TRAVEL),
            Term("海量", "大量", TermType.TRAVEL),
            Term("炸魚", "碾壓", TermType.TRAVEL),
            Term("塑料", "塑膠", TermType.TRAVEL),
            Term("高端", "高級", TermType.TRAVEL),
            Term("取消發車", "取消班次", TermType.TRAVEL),
            Term("停運", "停駛", TermType.TRAVEL),
            Term("改簽", "改票", TermType.TRAVEL),
            Term("取票機", "自動售票機", TermType.TRAVEL),
            Term("硬座", "對號座", TermType.TRAVEL),
            Term("硬臥", "臥舖", TermType.TRAVEL),
            Term("軟臥", "臥舖", TermType.TRAVEL),
            Term("空調車", "有冷氣的客運", TermType.TRAVEL),
            Term("行李艙", "行李置放區", TermType.TRAVEL),
            Term("商務座", "商務車廂", TermType.TRAVEL),

            # 飲食用語 (FOOD)
            Term("土豆", "馬鈴薯", TermType.FOOD),
            Term("番茄", "蕃茄", TermType.FOOD),
            Term("西紅柿", "蕃茄", TermType.FOOD),
            Term("玉米", "玉米", TermType.FOOD),
            Term("土豆", "花生", TermType.FOOD),
            Term("豆腐花", "豆花", TermType.FOOD),
            Term("果汁", "果菜汁", TermType.FOOD),
            Term("面條", "麵", TermType.FOOD),
            Term("餃子", "水餃", TermType.FOOD),
            Term("芝士", "起司", TermType.FOOD),
            Term("酸奶", "優格", TermType.FOOD),
            Term("黃油", "奶油", TermType.FOOD),
            
            # 科技術語 (TECHNICAL)
            Term("軟件", "軟體", TermType.TECHNICAL),
            Term("數據庫", "資料庫", TermType.TECHNICAL),
            Term("服務器", "伺服器", TermType.TECHNICAL),
            Term("文件夾", "資料夾", TermType.TECHNICAL),
            Term("鼠標", "滑鼠", TermType.TECHNICAL),
            Term("程序", "程式", TermType.TECHNICAL),
            Term("界面", "介面", TermType.TECHNICAL),
            Term("互聯網", "網際網路", TermType.TECHNICAL),
            Term("排錯", "除錯", TermType.TECHNICAL),
            Term("視頻", "影片", TermType.TECHNICAL),
            Term("內存", "記憶體", TermType.TECHNICAL),
            Term("硬盤", "硬碟", TermType.TECHNICAL),
            Term("操作系統", "作業系統", TermType.TECHNICAL),
            Term("搜索引擎", "搜尋引擎", TermType.TECHNICAL),
            Term("人工智能", "人工智慧", TermType.TECHNICAL),
            Term("芯片", "晶片", TermType.TECHNICAL),
            Term("電算機", "電腦", TermType.TECHNICAL),
            Term("刷屏", "洗版", TermType.TECHNICAL),
            Term("套路", "手法", TermType.TECHNICAL),
            Term("U盤", "USB 隨身碟", TermType.TECHNICAL),
            Term("YYDS", "令人崇拜", TermType.TECHNICAL),
            Term("博客", "部落格", TermType.TECHNICAL),
            Term("屏幕", "螢幕", TermType.TECHNICAL),
            Term("刷新", "重新整理", TermType.TECHNICAL),
            Term("全屏", "全螢幕", TermType.TECHNICAL),
            Term("兼容", "相容", TermType.TECHNICAL),
            Term("分辨率", "解析度", TermType.TECHNICAL),
            Term("程序", "程式", TermType.TECHNICAL),
            Term("芯片", "晶片", TermType.TECHNICAL),
            Term("適配器", "變壓器", TermType.TECHNICAL),
            Term("集成電路", "積體電路", TermType.TECHNICAL),
            Term("搖控器", "遙控器", TermType.TECHNICAL),
            Term("打印", "列印", TermType.TECHNICAL),
            Term("複印", "影印", TermType.TECHNICAL),
            Term("光盤", "光碟", TermType.TECHNICAL),
            Term("激活", "啟動", TermType.TECHNICAL),
            Term("博客", "部落格", TermType.TECHNICAL),

            # 生活常用語 (GENERAL) - 稱謂
            Term("先生", "先生", TermType.GENERAL),
            Term("女士", "小姐", TermType.GENERAL),
            Term("老師", "老師", TermType.GENERAL),

            # 網路用語 (INTERNET)
            Term("點贊", "按讚", TermType.INTERNET),
            Term("在線", "上線", TermType.INTERNET),
            Term("博客", "部落格", TermType.INTERNET),
            Term("聊天", "聊天室", TermType.INTERNET),
            Term("屏蔽", "封鎖", TermType.INTERNET),
            Term("上傳", "上載", TermType.INTERNET),
            Term("下載", "下載", TermType.INTERNET),
            Term("表情包", "貼圖", TermType.INTERNET),
            Term("轉發", "分享", TermType.INTERNET),
            Term("朋友圈", "動態", TermType.INTERNET),
            Term("見不得人", "隱藏", TermType.INTERNET),
            Term("套路", "老梗", TermType.INTERNET),
            Term("打錢", "轉帳", TermType.INTERNET),
            Term("智能", "智慧", TermType.INTERNET),
            Term("幹你娘", "幹你娘", TermType.INTERNET),
            Term("磨叨", "囉唆", TermType.INTERNET),
            Term("默認", "預設", TermType.INTERNET),
            Term("音頻", "音訊", TermType.INTERNET),

            # 地名用語 (PLACE)
            Term("臺北市", "台北市", TermType.PLACE),
            Term("臺中市", "台中市", TermType.PLACE),
            Term("臺南市", "台南市", TermType.PLACE),
            Term("臺灣", "台灣", TermType.PLACE),
            Term("中國臺灣", "台灣", TermType.PLACE),
            Term("中國大陸", "中國", TermType.PLACE),
            Term("香港特別行政區", "香港", TermType.PLACE),
            Term("澳門特別行政區", "澳門", TermType.PLACE),
            
            # 國際都市
            Term("朝鮮", "北韓", TermType.PLACE),
            Term("悉尼", "雪梨", TermType.PLACE),
            Term("老撾", "寮國", TermType.PLACE),
            Term("悉尼", "雪梨", TermType.PLACE),           # Sydney 的不同音譯
            Term("三藩市", "舊金山", TermType.PLACE),       # San Francisco 的不同稱呼
            Term("巴塞羅那", "巴塞隆納", TermType.PLACE),    # Barcelona 的不同音譯

            # 中東地區
            Term("迪拜", "杜拜", TermType.PLACE),           # Dubai 的不同音譯
            Term("阿布扎比", "阿布達比", TermType.PLACE),    # Abu Dhabi 的不同音譯
            Term("多哈", "杜哈", TermType.PLACE),           # Doha 的不同音譯
            
            # 著名景點
            Term("泰姬陵", "泰姬瑪哈陵", TermType.PLACE),    # 簡稱與全稱差異
            Term("迪拜塔", "哈里發塔", TermType.PLACE),     # 通稱與正式名稱
            Term("埃及金字塔", "吉薩金字塔", TermType.PLACE), # 通稱與特定稱呼
            Term("西貢", "胡志明市", TermType.PLACE),       # 舊稱與新稱
            
            # 重要地標
            Term("凡爾賽宮", "凡爾賽宮殿", TermType.PLACE),  # 簡稱與正式名稱

            # 著名機場
            Term("希斯路機場", "希斯洛機場", TermType.PLACE), # Heathrow 的不同音譯
            Term("戴高樂機場", "戴高樂機場", TermType.PLACE), # Charles de Gaulle 的統一譯名
            
            # 自然景觀
            Term("維多利亞瀑布", "維多利亞瀑布", TermType.PLACE),   # Victoria Falls 的統一譯名
            Term("尼亞加拉瀑布", "尼加拉瀑布", TermType.PLACE),    # Niagara Falls 的不同音譯
            Term("厄佛勒斯峰", "聖母峰", TermType.PLACE),         # Mount Everest 的不同稱呼
            
            # 特殊地區
            Term("荷里活", "好萊塢", TermType.PLACE),       # Hollywood 的不同音譯

            # 島嶼
            Term("巴里島", "峇里島", TermType.PLACE),       # Bali 的不同音譯
            Term("帛琉", "帛琉", TermType.PLACE),           # Palau 的統一譯名
            Term("馬爾地夫", "馬爾地夫", TermType.PLACE),     # Maldives 的統一譯名

            # 人名用語 (PERSON)
            Term("特朗普", "川普", TermType.PERSON),
            Term("普京", "普丁", TermType.PERSON),
            Term("奧巴馬", "歐巴馬", TermType.PERSON),
            Term("希拉里", "希拉蕊", TermType.PERSON),
            Term("克林頓", "柯林頓", TermType.PERSON),
            Term("布什", "布希", TermType.PERSON),
            Term("馬克龍", "馬克宏", TermType.PERSON),
            Term("葉利欽", "葉爾欽", TermType.PERSON),
            Term("馬哈蒂爾", "馬哈迪", TermType.PERSON),
            Term("蘇卡諾", "蘇加諾", TermType.PERSON),
            Term("甘迺迪", "甘迺迪", TermType.PERSON),
            Term("尼克松", "尼克森", TermType.PERSON),
            Term("丘吉爾", "邱吉爾", TermType.PERSON),
            Term("拿破崙", "拿破崙", TermType.PERSON),
            Term("巴赫", "巴哈", TermType.PERSON),
            Term("布拉德皮特", "布萊德彼特", TermType.PERSON),

            # TPE-桃園機場專用
            Term("端子ㄧ", "第一航廈", TermType.TPE),
            Term("端子二", "第二航廈", TermType.TPE),
            Term("端子三", "第三航廈", TermType.TPE),
            Term("端子1", "第一航廈", TermType.TPE),
            Term("端子2", "第二航廈", TermType.TPE),
            Term("端子3", "第三航廈", TermType.TPE),
            Term("端子 1", "第一航廈", TermType.TPE),
            Term("端子 2", "第二航廈", TermType.TPE),
            Term("端子 3", "第三航廈", TermType.TPE),


            Term("終端ㄧ", "第一航廈", TermType.TPE),
            Term("終端二", "第二航廈", TermType.TPE),
            Term("終端三", "第三航廈", TermType.TPE),
            Term("終端1", "第一航廈", TermType.TPE),
            Term("終端2", "第二航廈", TermType.TPE),
            Term("終端3", "第三航廈", TermType.TPE),
            Term("終端 1", "第一航廈", TermType.TPE),
            Term("終端 2", "第二航廈", TermType.TPE),
            Term("終端 3", "第三航廈", TermType.TPE),

            Term("ㄧ號終端站", "第一航廈", TermType.TPE),
            Term("二號終端站", "第二航廈", TermType.TPE),
            Term("三號終端站", "第三航廈", TermType.TPE),
            Term("1號終端站", "第一航廈", TermType.TPE),
            Term("2號終端站", "第二航廈", TermType.TPE),
            Term("3號終端站", "第三航廈", TermType.TPE),
            Term("1 號終端站", "第一航廈", TermType.TPE),
            Term("2 號終端站", "第二航廈", TermType.TPE),
            Term("3 號終端站", "第三航廈", TermType.TPE),

            Term("ㄧ號客運大樓", "第一航廈", TermType.TPE),
            Term("二號客運大樓", "第二航廈", TermType.TPE),
            Term("三號客運大樓", "第三航廈", TermType.TPE),
            Term("1號客運大樓", "第一航廈", TermType.TPE),
            Term("2號客運大樓", "第二航廈", TermType.TPE),
            Term("3號客運大樓", "第三航廈", TermType.TPE),
            Term("1 號客運大樓", "第一航廈", TermType.TPE),
            Term("2 號客運大樓", "第二航廈", TermType.TPE),
            Term("3 號客運大樓", "第三航廈", TermType.TPE),

            Term("ㄧ號航廈", "第一航廈", TermType.TPE),
            Term("二號航廈", "第二航廈", TermType.TPE),
            Term("三號航廈", "第三航廈", TermType.TPE),
            Term("1號航廈", "第一航廈", TermType.TPE),
            Term("2號航廈", "第二航廈", TermType.TPE),
            Term("3號航廈", "第三航廈", TermType.TPE),
            Term("1 號航廈", "第一航廈", TermType.TPE),
            Term("2 號航廈", "第二航廈", TermType.TPE),
            Term("3 號航廈", "第三航廈", TermType.TPE),

            Term("1號航站樓", "第一航廈", TermType.TPE),
            Term("2號航站樓", "第二航廈", TermType.TPE),
            Term("3號航站樓", "第三航廈", TermType.TPE),
            Term("1 號航站樓", "第一航廈", TermType.TPE),
            Term("2 號航站樓", "第二航廈", TermType.TPE),
            Term("3 號航站樓", "第三航廈", TermType.TPE),
            Term("ㄧ號航站樓", "第一航廈", TermType.TPE),
            Term("二號航站樓", "第二航廈", TermType.TPE),
            Term("三號航站樓", "第三航廈", TermType.TPE),
        ]
        self.add_terms(default_terms)
    
    def _build_automaton(self) -> None:
        """建構 AC 自動機"""
        self.automaton = ahocorasick.Automaton()
        for cn_term, term in self.terms.items():
            self.automaton.add_word(cn_term, (cn_term, term.tw_term))
        self.automaton.make_automaton()
    
    def add_terms(self, new_terms: List[Term]) -> None:
        """新增術語"""
        for term in new_terms:
            self.terms[term.cn_term] = term
        self.version += 1
        self.last_updated = time.time()
        self._build_automaton()
    
    def convert_text(self, text: str) -> str:
        """轉換文本中的術語"""
        if not text.strip() or not self.terms:
            return text
            
        # 找出所有匹配
        matches = []
        for end_index, (original, replacement) in self.automaton.iter(text):
            start_index = end_index - len(original) + 1
            matches.append((start_index, end_index, original, replacement))
        
        # 處理重疊
        matches.sort(key=lambda x: (-x[0], -(x[1] - x[0])))
        result = list(text)
        used_positions = set()
        
        for start, end, original, replacement in sorted(matches):
            if not any(i in used_positions for i in range(start, end + 1)):
                result[start:end + 1] = replacement
                used_positions.update(range(start, end + 1))
        
        return ''.join(result)