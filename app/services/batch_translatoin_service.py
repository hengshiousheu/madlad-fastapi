from typing import List, Dict, Optional
from app.services.dependency_injection import get_deepl_translator_service, get_google_translator_service, get_bridging_translator_service, get_madlad400_translator_service
from fastapi import Depends
from loguru import logger
import asyncio
from app.services.madlad400_translator import Madlad400TranslatorService
from app.strategy.strategies import TranslationStrategyFactory
from app.strategy.enum import TranslationStrategyEnum
class BatchTranslationService:
    
    _instance = None

    def __new__(cls, *args, **kwargs):
        if cls._instance is None:
            cls._instance = super().__new__(cls)
        return cls._instance

    def __init__(self, 
                 strategy_factory: TranslationStrategyFactory
                ):
        """
        初始化 BatchTranslationService。

        :param custom_translator: Madlad400TranslatorService, 自研翻譯服務
        """
        self.strategy_factory = strategy_factory

    async def _translate_with_strategy(self, strategy: TranslationStrategyEnum, text: str, target_language: str, source_language: str) -> str:
        return await asyncio.to_thread(strategy.translate, text, target_language, source_language)

    async def translate_multiple_languages(
            self, 
            input_text: str, 
            languages: List[str], 
            source_language: str,
            strategy_name: Optional[TranslationStrategyEnum] = TranslationStrategyEnum.DEFAULT
        ) -> Dict[str, str]:
        """
        同时翻译文本到多种语言。

        :param input_text: str, 要翻譯的文本
        :param languages: List[str], 目標語言列表
        :param strategy_name: TranslationStrategyEnum, 使用的翻譯策略
        :return: Dict[str, str], 每種語言的翻譯結果
        """
        logger.info(f"將文本翻譯成多種語言: {languages}")
        results = {}

        tasks = []
        for lang in languages:
            translation_strategy = self.strategy_factory.create_strategy(strategy_name)
            tasks.append(self._translate_with_strategy(translation_strategy, input_text, lang, source_language))

        # 同時運行所有翻譯任務
        translations = await asyncio.gather(*tasks, return_exceptions=True)

        # 處理翻譯結果
        for lang, translation in zip(languages, translations):
            if isinstance(translation, Exception):
                logger.error(f"翻譯 ({lang}) 失敗: {translation}")
                results[lang] = f"翻譯 ({lang}) 失敗: {translation}"  # 或者其他的錯誤處理
            else:
                results[lang] = translation

        return results

def get_translation_strategy_factory(
    deepl_service=Depends(get_deepl_translator_service),
    google_service=Depends(get_google_translator_service),
    madlad400_service=Depends(get_madlad400_translator_service),
    bridging_service=Depends(get_bridging_translator_service)
) -> TranslationStrategyFactory:
    return TranslationStrategyFactory(deepl_service, google_service, madlad400_service, bridging_service)

def get_batch_translation_service(
    strategy_factory: TranslationStrategyFactory = Depends(get_translation_strategy_factory)
) -> BatchTranslationService:
    return BatchTranslationService(strategy_factory)
