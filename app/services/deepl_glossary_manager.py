import deepl
from typing import Dict, List, Optional
from loguru import logger

class GlossaryManager:
    """
    GlossaryManager 類別負責處理所有與 DeepL 詞彙表相關的操作。
    它提供了建立、取得、列舉和刪除詞彙表的方法，
    以及管理特定領域詞彙表的功能。
    """

    def __init__(self, translator: deepl.Translator):
        """
        初始化 GlossaryManager，需要一個 DeepL 翻譯器實例。

        :param translator: deepl.Translator 的實例
        """
        self.translator = translator
        # 用於將領域名稱對應到詞彙表 ID 的字典
        self.domain_glossaries: Dict[str, str] = {}

    def create_glossary(self, name: str, source_lang: str, target_lang: str, entries: Dict[str, str], domain: Optional[str] = None) -> deepl.GlossaryInfo:
        """
        建立新的詞彙表，並可選擇將其與特定領域關聯。

        :param name: 詞彙表名稱
        :param source_lang: 來源語言代碼
        :param target_lang: 目標語言代碼
        :param entries: 詞彙表條目的字典 (來源詞: 目標詞)
        :param domain: 可選的關聯領域
        :return: 包含已建立詞彙表詳細資訊的 GlossaryInfo 物件
        """
        try:
            # 使用 DeepL API 建立詞彙表
            glossary = self.translator.create_glossary(name, source_lang, target_lang, entries)
            
            # 如果指定了領域，則將詞彙表 ID 與該領域關聯
            if domain:
                self.domain_glossaries[domain] = glossary.glossary_id
            
            logger.info(f"已成功建立詞彙表 '{name}'，領域為 '{domain}'，ID 為: {glossary.glossary_id}")
            return glossary
        except deepl.DeepLException as e:
            logger.error(f"建立詞彙表失敗: {e}")
            raise

    def get_glossary(self, glossary_id: str) -> deepl.GlossaryInfo:
        """
        透過 ID 取得特定詞彙表。

        :param glossary_id: 要取得的詞彙表 ID
        :return: 包含所請求詞彙表詳細資訊的 GlossaryInfo 物件
        """
        try:
            return self.translator.get_glossary(glossary_id)
        except deepl.DeepLException as e:
            logger.error(f"取得詞彙表失敗: {e}")
            raise

    def list_glossaries(self) -> List[deepl.GlossaryInfo]:
        """
        列舉與 DeepL 帳戶關聯的所有詞彙表。

        :return: GlossaryInfo 物件的列表
        """
        try:
            return self.translator.list_glossaries()
        except deepl.DeepLException as e:
            logger.error(f"列舉詞彙表失敗: {e}")
            raise

    def delete_glossary(self, glossary_id: str) -> None:
        """
        透過 ID 刪除特定詞彙表，並移除任何領域關聯。

        :param glossary_id: 要刪除的詞彙表 ID
        """
        try:
            self.translator.delete_glossary(glossary_id)
            # 如果存在，從領域對應中移除該詞彙表
            self.domain_glossaries = {k: v for k, v in self.domain_glossaries.items() if v != glossary_id}
            logger.info(f"已成功刪除 ID 為 {glossary_id} 的詞彙表")
        except deepl.DeepLException as e:
            logger.error(f"刪除詞彙表失敗: {e}")
            raise

    def get_glossary_entries(self, glossary_id: str) -> Dict[str, str]:
        """
        取得特定詞彙表的條目。

        :param glossary_id: 要取得條目的詞彙表 ID
        :return: 詞彙表條目的字典 (來源詞: 目標詞)
        """
        try:
            return self.translator.get_glossary_entries(glossary_id)
        except deepl.DeepLException as e:
            logger.error(f"取得詞彙表條目失敗: {e}")
            raise

    def get_domain_glossary(self, domain: str) -> Optional[str]:
        """
        取得與特定領域關聯的詞彙表 ID。

        :param domain: 要查詢的領域
        :return: 如果找到則返回詞彙表 ID，否則返回 None
        """
        return self.domain_glossaries.get(domain)