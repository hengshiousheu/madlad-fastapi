import yaml
import csv
from app.services.deepl_translator import DeepLTranslatorService
import argparse
import re

def load_glossaries(file_path):
    """
    從 YAML 文件中加載詞彙表配置。

    參數:
    file_path (str): YAML 配置文件的路徑。

    返回:
    dict: 包含所有詞彙表配置的字典。
    """
    with open(file_path, 'r', encoding='utf-8') as file:
        return yaml.safe_load(file)

def parse_csv_header(header_row):
    """
    解析 CSV 文件的標題行，提取語言和對應的列索引。

    參數:
    header_row (list): CSV 文件的標題行。

    返回:
    dict: 語言代碼到列索引的映射。
    """
    lang_map = {}
    for index, column in enumerate(header_row):
        match = re.match(r'(.+)-(\w+)', column)
        if match:
            lang_code = match.group(2)
            lang_map[lang_code] = index
    return lang_map

def load_csv_entries(csv_file, source_lang, target_lang):
    """
    从 CSV 文件中加载特定语言对的词条。

    参数:
    csv_file (str): CSV 文件的路径。
    source_lang (str): 源语言的代号。
    target_lang (str): 目标语言的代号。

    返回:
    dict: 源语言术语到目标语言术语的映射字典。
    """
    entries = {}
    with open(csv_file, 'r', encoding='utf-8') as file:
        csv_reader = csv.reader(file)
        header = next(csv_reader)
        lang_map = parse_csv_header(header)
        
        if source_lang not in lang_map or target_lang not in lang_map:
            raise ValueError(f"未找到源语言 {source_lang} 或目标语言 {target_lang}")

        source_index = lang_map[source_lang]
        target_index = lang_map[target_lang]

        for row_num, row in enumerate(csv_reader, start=2):  # 从2开始，因为第一行是header
            if len(row) > max(source_index, target_index):
                source_term = row[source_index].strip()
                target_term = row[target_index].strip()
                if source_term and target_term:
                    entries[source_term] = target_term
                else:
                    print(f"在第 {row_num} 行跳过空的源或目标术语")
            else:
                print(f"在第 {row_num} 行跳过不完整的行")
    return entries

def create_or_update_glossaries(translator_service, glossaries):
    """
    创建或更新 DeepL 词汇表。

    参数:
    translator_service (DeepLTranslatorService): DeepL 翻译服务的实例。
    glossaries (dict): 包含所有词汇表配置的字典。
    """
    for glossary_config in glossaries['glossaries']:
        csv_file = glossary_config['csv_file']
        language_pairs = glossary_config['language_pairs']
        
        for source_lang, target_langs in language_pairs.items():
            for target_lang in target_langs:
                name = f"{glossary_config['name']}_{source_lang}_{target_lang}"
                try:
                    entries = load_csv_entries(csv_file, source_lang, target_lang)
                    
                    if not entries:
                        print(f"词汇表 {name} 中没有有效的词条，已跳过。")
                        continue  # 跳过没有词条的词汇表

                    existing_glossary = next(
                        (g for g in translator_service.list_glossaries() if g.name == name),
                        None
                    )
                    if existing_glossary:
                        print(f"更新词汇表: {name}")
                        translator_service.delete_glossary(existing_glossary.glossary_id)
                    else:
                        print(f"创建词汇表: {name}")
                    
                    print(f"词汇表 {name} 包含 {len(entries)} 个词条。")

                    translator_service.create_glossary(
                        name=name,
                        source_lang=source_lang,
                        target_lang=target_lang,
                        entries=entries,
                        domain=glossary_config.get('domain')
                    )
                except Exception as e:
                    print(f"处理词汇表 {name} 时出错: {str(e)}")


def main():
    """
    主函數，解析命令行參數並執行詞彙表管理流程。
    """
    parser = argparse.ArgumentParser(description="管理 DeepL 詞彙表")
    parser.add_argument("--config", default="glossaries.yaml", help="詞彙表配置文件路徑")
    args = parser.parse_args()

    translator_service = DeepLTranslatorService()
    glossaries = load_glossaries(args.config)
    create_or_update_glossaries(translator_service, glossaries)

if __name__ == "__main__":
    #python3 -m app.services.manage_deepl_glossaries --config ./app/services/glossaries.yaml
    main()