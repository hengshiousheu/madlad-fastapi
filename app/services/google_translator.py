from google.oauth2 import service_account
from google.cloud import translate_v2 as translate
from app.core.config import settings
from loguru import logger

class GoogleTranslateService:

    _instance = None

    def __new__(cls, *args, **kwargs):
        if cls._instance is None:
            cls._instance = super().__new__(cls)
        return cls._instance

    def __init__(self, project_id: str):
        try:
            credentials = service_account.Credentials.from_service_account_info(
                settings.GOOGLE_APPLICATION_CREDENTIALS_JSON
            )
            self.client = translate.Client(credentials=credentials)
            self.project_id = project_id
            logger.info("Google Translate Client successfully initialized")
        except Exception as e:
            logger.error(f"Failed to initialize Google Translate Client: {e}")
            raise

    def translate(self, text: str, target_language: str, source_language: str) -> str:
        if not text:
            logger.error("No text to translate provided")
            raise ValueError("No text to translate")
        if not target_language:
            logger.error("target languages must be specified")
            raise ValueError("target languages must be specified")
        
        try:
            result = self.client.translate(
                text,
                source_language=source_language,
                target_language=target_language
            )
            logger.info(f"Translation successful: {result['translatedText']}")
            return result['translatedText']
        except Exception as e:
            logger.error(f"Failed to translate text: {e}")
            raise RuntimeError(f"Failed to translate text due to: {e}")