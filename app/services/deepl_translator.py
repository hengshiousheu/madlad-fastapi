import deepl
from app.core.config import settings
from opencc import OpenCC
from loguru import logger
from typing import List, Dict, Tuple, Optional
from app.services.deepl_glossary_manager import GlossaryManager

import re
from difflib import SequenceMatcher
from typing import Union, BinaryIO, Tuple, Dict, List
import json
import os
class DeepLTranslatorService:
    """
    DeepL 翻譯服務類，提供基於 DeepL API 的翻譯功能，支持使用預加載的詞彙表（glossaries）。

    這個類在初始化時會加載所有可用的詞彙表，並在翻譯過程中使用適當的詞彙表來提高翻譯質量。
    它還支持將簡體中文轉換為繁體中文。

    屬性:
        auth_key (str): DeepL API 的認證密鑰。
        opencc (OpenCC): 用於簡繁轉換的 OpenCC 實例。
        translator (deepl.Translator): DeepL 翻譯器實例。
        glossaries (Dict[Tuple[str, str, str], str]): 存儲詞彙表信息的字典，
                                                      鍵為 (domain, source_lang, target_lang)，值為 glossary_id。
    """

    SUPPORTED_LANGUAGES: List[str] = [
        'ZH', 'EN','EN-US', 'EN-GB', 'JA', 'KO', 'ID', 'ES', 'IT', 'DE', 'NL', 'PT-BR', 'PT-PT', 'FR', 'PL', 'RU', 'TR', 'SV'
    ]

    _instance = None

    def __new__(cls, *args, **kwargs):
        """
        確保只建立 DeepLTranslatorService 的一個實例（單例模式）。
        """
        if cls._instance is None:
            cls._instance = super().__new__(cls)
        return cls._instance
    
    class ScriptDatabase:
        """腳本資料庫管理類"""
        def __init__(self, scripts_path: str = "scripts/"):
            self.scripts_path = scripts_path
            self.scripts: Dict[str, Dict] = {}
            self._load_scripts()

        def _load_scripts(self):
            """載入所有腳本"""
            os.makedirs(self.scripts_path, exist_ok=True)
            
            # 北北基好玩卡場景
            fun_pass_script = {
                "name": "fun_pass",
                "translations": {
                    "zh_to_ja": {
                        "您好，我想購買「北北基好玩卡」，可以請您告訴我該怎麼買嗎？": 
                            "こんにちは、「北北基好玩卡（Taipei FunPASS）」を購入したいのですが、購入方法を教えていただけますか？",
                        
                        "您好，目前我們有6種不同類型的「北北基好玩卡」，每種卡別的優惠內容略有不同，您可以先參考這個網站，裡面有詳細的介紹和比較，讓您更方便選擇。":
                            "はい、現在6種類の「北北基好玩卡（Taipei FunPASS）」をご用意しております。それぞれ特典内容が若干異なります。このウェブサイトで詳しい説明と比較をご覧いただけますので、お選びの際の参考にしていただければと思います。",
                        
                        "我不太清楚每種卡的差異，我想直接買票，該怎麼辦？":
                            "各カードの違いがよく分からないのですが、直接チケットを購入したいのですが、どうすればよいですか？",
                        
                        "如果您想現場購買，目前可以在市府轉運站的悠遊卡客服中心購買。他們會協助您辦理，並解答您對卡片的任何問題。":
                            "その場でご購入をご希望の場合は、市政府バスターミナルのEasy Card（悠遊卡）カスタマーサービスセンターでお買い求めいただけます。スタッフがお手続きのお手伝いをさせていただき、カードに関するご質問にもお答えいたします。"
                    },
                    "ja_to_zh": {
                        "こんにちは、「北北基好玩卡（Taipei FunPASS）」を購入したいのですが、購入方法を教えていただけますか？":
                            "您好，我想購買「北北基好玩卡」，可以請您告訴我該怎麼買嗎？",
                        
                        "はい、現在6種類の「北北基好玩卡（Taipei FunPASS）」をご用意しております。それぞれ特典内容が若干異なります。このウェブサイトで詳しい説明と比較をご覧いただけますので、お選びの際の参考にしていただければと思います。":
                            "您好，目前我們有6種不同類型的「北北基好玩卡」，每種卡別的優惠內容略有不同，您可以先參考這個網站，裡面有詳細的介紹和比較，讓您更方便選擇。",
                        
                        "各カードの違いがよく分からないのですが、直接チケットを購入したいのですが、どうすればよいですか？":
                            "我不太清楚每種卡的差異，我想直接買票，該怎麼辦？",
                        
                        "その場でご購入をご希望の場合は、市政府バスターミナルのEasy Card（悠遊卡）カスタマーサービスセンターでお買い求めいただけます。スタッフがお手続きのお手伝いをさせていただき、カードに関するご質問にもお答えいたします。":
                            "如果您想現場購買，目前可以在市府轉運站的悠遊卡客服中心購買。他們會協助您辦理，並解答您對卡片的任何問題。"
                    }
                }
            }
            
            # 捷運場景
            mrt_script = {
                "name": "mrt",
                "translations": {
                    "zh_to_ja": {
                        "不好意思，請問去台北101要怎麼走呢？":
                            "すみません、台北101へはどう行けばよいですか？",
                        
                        "您好，您可以搭乘捷運紅線，它會直接帶您到台北101站，非常方便。":
                            "地下鉄の赤線をご利用いただくと、台北101駅に直接到着できて大変便利です。",
                        
                        "那捷運紅線的入口在哪裡呢？我有點迷路了。":
                            "その地下鉄赤線の入口はどこにありますか？少し道に迷ってしまって。",
                        
                        "沒問題，您往後方直走，大約100公尺後就能看到捷運站的入口。":
                            "大丈夫です。後ろに向かってまっすぐ約100メートル進むと、地下鉄の入口が見えてきます。",
                        
                        "請問捷運營運到幾點呢？我怕來不及回程。":
                            "地下鉄は何時まで運行していますか？帰りに間に合うか心配で。",
                        
                        "台北捷運的末班車大約是凌晨12點，建議您提前些時間，避免錯過末班車。":
                            "台北地下鉄の最終電車は午前0時頃です。最終電車に乗り遅れないよう、少し早めの時間に行動されることをお勧めします。"
                    },
                    "ja_to_zh": {
                        "すみません、台北101へはどう行けばよいですか？":
                            "不好意思，請問去台北101要怎麼走呢？",
                        
                        "地下鉄の赤線をご利用いただくと、台北101駅に直接到着できて大変便利です。":
                            "您好，您可以搭乘捷運紅線，它會直接帶您到台北101站，非常方便。",
                        
                        "その地下鉄赤線の入口はどこにありますか？少し道に迷ってしまって。":
                            "那捷運紅線的入口在哪裡呢？我有點迷路了。",
                        
                        "大丈夫です。後ろに向かってまっすぐ約100メートル進むと、地下鉄の入口が見えてきます。":
                            "沒問題，您往後方直走，大約100公尺後就能看到捷運站的入口。",
                        
                        "地下鉄は何時まで運行していますか？帰りに間に合うか心配で。":
                            "請問捷運營運到幾點呢？我怕來不及回程。",
                        
                        "台北地下鉄の最終電車は午前0時頃です。最終電車に乗り遅れないよう、少し早めの時間に行動されることをお勧めします。":
                            "台北捷運的末班車大約是凌晨12點，建議您提前些時間，避免錯過末班車。"
                    }
                }
            }

            # 郵局場景腳本
            post_office_script = {
                "name": "post_office",
                "translations": {
                    "zh_to_ja": {
                        # 原有對話
                        "您好，請問我在哪裡可以找到郵局呢？我有些文件需要寄送。": 
                            "すみません、郵便局はどこにありますか？書類を送りたいのですが。",
                        
                        "您好！請您往右前方走約兩分鐘，郵局就在您左手邊，沿途會有指示牌，您不會錯過的。":
                            "右前方に2分ほど歩いていただくと、左手に郵便局がございます。途中に案内表示がありますので、お分かりいただけると思います。",
                        
                        "謝謝您！我還想知道，郵局的服務時間是怎麼樣的？":
                            "ありがとうございます！営業時間も教えていただけますか？",
                        
                        "郵局服務到幾點呢？我希望能在今天寄出。":
                            "郵便局は何時まで営業していますか？今日中に送りたいのですが。",
                        
                        "郵局的營業時間是週一到週五，服務到下午五點。周六則只營業到中午12點，而星期天是不營業的。如果您想寄的文件比較急，建議您今天盡快過去。":
                            "郵便局は月曜から金曜は午後5時まで営業しています。土曜は正午12時まで、日曜は休業です。急ぎの書類でしたら、今日のなるべく早い時間に行くことをお勧めします。",
                        
                        "明白了，謝謝您的幫助！還有，郵局裡有提供其他服務嗎？例如包裹寄送或是掛號郵件？":
                            "分かりました、ありがとうございます！他にどんなサービスがありますか？例えば荷物の配送や書留などは？",
                        
                        "是的，郵局不僅提供一般郵寄服務，還有包裹寄送、掛號郵件等多種服務。如果您需要寄送貴重物品，我們建議使用掛號郵件，以確保安全送達。":
                            "はい、通常の郵便サービスの他に、荷物の配送や書留など様々なサービスをご用意しております。貴重品を送られる場合は、安全な配達のため書留サービスをお勧めしております。",
                        
                        "太好了，我會考慮使用掛號服務的。再次感謝您的協助！":
                            "それは良かったです。書留サービスを検討してみます。ご協力ありがとうございました！"
                    },
                    "ja_to_zh": {
                        # 日文到中文的對照（反向對應）
                        "すみません、郵便局はどこにありますか？書類を送りたいのですが。":
                            "您好，請問我在哪裡可以找到郵局呢？我有些文件需要寄送。",
                        
                        "右前方に2分ほど歩いていただくと、左手に郵便局がございます。途中に案内表示がありますので、お分かりいただけると思います。":
                            "您好！請您往右前方走約兩分鐘，郵局就在您左手邊，沿途會有指示牌，您不會錯過的。",
                        
                        "ありがとうございます！営業時間も教えていただけますか？":
                            "謝謝您！我還想知道，郵局的服務時間是怎麼樣的？",
                        
                        "郵便局は何時まで営業していますか？今日中に送りたいのですが。":
                            "郵局服務到幾點呢？我希望能在今天寄出。",
                        
                        "郵便局は月曜から金曜は午後5時まで営業しています。土曜は正午12時まで、日曜は休業です。急ぎの書類でしたら、今日のなるべく早い時間に行くことをお勧めします。":
                            "郵局的營業時間是週一到週五，服務到下午五點。周六則只營業到中午12點，而星期天是不營業的。如果您想寄的文件比較急，建議您今天盡快過去。",
                        
                        "分かりました、ありがとうございます！他にどんなサービスがありますか？例えば荷物の配送や書留などは？":
                            "明白了，謝謝您的幫助！還有，郵局裡有提供其他服務嗎？例如包裹寄送或是掛號郵件？",
                        
                        "はい、通常の郵便サービスの他に、荷物の配送や書留など様々なサービスをご用意しております。貴重品を送られる場合は、安全な配達のため書留サービスをお勧めしております。":
                            "是的，郵局不僅提供一般郵寄服務，還有包裹寄送、掛號郵件等多種服務。如果您需要寄送貴重物品，我們建議使用掛號郵件，以確保安全送達。",
                        
                        "それは良かったです。書留サービスを検討してみます。ご協力ありがとうございました！":
                            "太好了，我會考慮使用掛號服務的。再次感謝您的協助！"
                    }
                }
            }

            # 計程車場景（整合所有計程車相關對話）
            taxi_script = {
                "name": "taxi",
                "translations": {
                    "zh_to_ja": {
                        # 第一組對話
                        "您好，請問我在哪裡可以叫計程車呢？": 
                            "すみません、タクシーはどこで呼べますか？",
                        
                        "您好！您可以往右前方直走，當您到達出口後，就會看到計程車招呼站，隨時都有計程車在那裡等候。":
                            "こんにちは！右前方をまっすぐ進んでいただくと、出口に到着後、タクシー乗り場が見えます。タクシーが常時待機しております。",
                        
                        "了解了，謝謝您！我稍後就過去。":
                            "分かりました、ありがとうございます！では、これから向かいます。",
                        
                        "我想知道，計程車通常需要等多久呢？":
                            "タクシーは通常どのくらい待つ必要がありますか？",
                        
                        "一般來說，在招呼站等的話，通常不會等太久，通常在五分鐘內就能搭上車。如果您有急事，也可以考慮使用叫車應用程式，這樣會更方便快捷。":
                            "通常、乗り場でお待ちいただく場合は、それほど長くお待ちいただく必要はありません。だいたい5分以内には乗車できます。もしお急ぎでしたら、配車アプリのご利用もご検討いただけます。そちらの方が便利で早いかと思います。",
                        
                        "好的，謝謝您提供的資訊！":
                            "はい、情報ありがとうございます！",
                        
                        "不客氣！如果還有其他問題，隨時都可以來詢問我們喔！":
                            "どういたしまして！他にご質問がございましたら、いつでもお申し付けください！",

                        # 第二組對話
                        "您好！計程車招呼站就在東三門外面，您只需走到門口，右手邊就能看到招呼站。":
                            "はい、タクシー乗り場は東三門の外にございます。出口まで行っていただくと、右手に乗り場が見えます。",
                        
                        "好的，謝謝您！那大約需要等多久呢？":
                            "ありがとうございます！だいたいどのくらい待つ必要がありますか？",
                        
                        "通常在那裡等幾分鐘就會有計程車過來。如果您急著要走，也可以考慮使用手機叫車的應用程式，會更快。":
                            "通常数分程度でタクシーが来ます。お急ぎの場合は、スマートフォンの配車アプリをご利用いただくと、より早くご利用いただけます。",
                        
                        "謝謝您的建議，我會試試的！":
                            "アドバイスありがとうございます！試してみます！",
                        
                        "不客氣！如果還有其他需要幫忙的，隨時可以來詢問我們喔。":
                            "どういたしまして！他にお困りのことがございましたら、いつでもお申し付けください。"
                    },
                    "ja_to_zh": {
                        # 第一組對話
                        "すみません、タクシーはどこで呼べますか？": 
                            "您好，請問我在哪裡可以叫計程車呢？",
                        
                        "こんにちは！右前方をまっすぐ進んでいただくと、出口に到着後、タクシー乗り場が見えます。タクシーが常時待機しております。":
                            "您好！您可以往右前方直走，當您到達出口後，就會看到計程車招呼站，隨時都有計程車在那裡等候。",
                        
                        "分かりました、ありがとうございます！では、これから向かいます。":
                            "了解了，謝謝您！我稍後就過去。",
                        
                        "タクシーは通常どのくらい待つ必要がありますか？":
                            "我想知道，計程車通常需要等多久呢？",
                        
                        "通常、乗り場でお待ちいただく場合は、それほど長くお待ちいただく必要はありません。だいたい5分以内には乗車できます。もしお急ぎでしたら、配車アプリのご利用もご検討いただけます。そちらの方が便利で早いかと思います。":
                            "一般來說，在招呼站等的話，通常不會等太久，通常在五分鐘內就能搭上車。如果您有急事，也可以考慮使用叫車應用程式，這樣會更方便快捷。",
                        
                        "はい、情報ありがとうございます！":
                            "好的，謝謝您提供的資訊！",
                        
                        "どういたしまして！他にご質問がございましたら、いつでもお申し付けください！":
                            "不客氣！如果還有其他問題，隨時都可以來詢問我們喔！",

                        # 第二組對話
                        "はい、タクシー乗り場は東三門の外にございます。出口まで行っていただくと、右手に乗り場が見えます。":
                            "您好！計程車招呼站就在東三門外面，您只需走到門口，右手邊就能看到招呼站。",
                        
                        "ありがとうございます！だいたいどのくらい待つ必要がありますか？":
                            "好的，謝謝您！那大約需要等多久呢？",
                        
                        "通常数分程度でタクシーが来ます。お急ぎの場合は、スマートフォンの配車アプリをご利用いただくと、より早くご利用いただけます。":
                            "通常在那裡等幾分鐘就會有計程車過來。如果您急著要走，也可以考慮使用手機叫車的應用程式，會更快。",
                        
                        "アドバイスありがとうございます！試してみます！":
                            "謝謝您的建議，我會試試的！",
                        
                        "どういたしまして！他にお困りのことがございましたら、いつでもお申し付けください。":
                            "不客氣！如果還有其他需要幫忙的，隨時可以來詢問我們喔。"
                    }
                }
            }

            # 遺失物場景（整合所有遺失物相關對話）
            lost_found_script = {
                "name": "lost_found",
                "translations": {
                    "zh_to_ja": {
                        # 機場場景
                        "不好意思，我的東西不見了，您能幫我嗎？":
                            "すみません、私の荷物が見当たらないのですが、ご協力いただけますか？",
                        
                        "當然可以，您是想尋找遺失物中心嗎？這裡有相關的服務。":
                            "もちろんです。遺失物センターをお探しでしょうか？そちらのサービスをご案内できます。",
                        
                        "是的，我該怎麼做？":
                            "はい、どうすればよいでしょうか？",
                        
                        "您可以前往機場服務台，工作人員會幫助您處理遺失物品的問題，並提供必要的協助。":
                            "空港インフォメーションカウンターをご利用ください。スタッフが遺失物に関する対応をさせていただき、必要なサポートをご提供いたします。",
                        
                        "請問有電話可以聯絡的嗎？我希望能提前詢問一下。":
                            "事前に問い合わせできる電話番号はありますか？",
                        
                        "當然可以，您可以直接打給航警局詢問，他們的電話是02-8770-2666。他們會協助查詢遺失的物品。":
                            "はい、ございます。航空警察局に直接お電話いただけます。電話番号は02-8770-2666です。遺失物についての確認をしていただけます。",
                        
                        "非常感謝！我會馬上聯絡他們。":
                            "ありがとうございます！早速連絡してみます。",
                        
                        "不客氣！如果您有其他問題或需要幫助，隨時可以告訴我們喔！":
                            "どういたしまして！他にご質問やお困りのことがございましたら、いつでもお申し付けください！",

                        # 高鐵場景
                        "不好意思，我的東西不見了，請問該怎麼辦？":
                            "すみません、私の荷物が見当たらないのですが、どうすればよいでしょうか？",
                        
                        "您好，您是否需要尋找遺失物中心來幫您尋回物品呢？":
                            "こんにちは。遺失物センターで荷物をお探しするお手伝いをさせていただけますが、いかがでしょうか？",
                        
                        "是的，我應該去哪裡找？":
                            "はい、どこに行けばよいですか？",
                        
                        "您可以前往高鐵的服務櫃台，他們會幫助您處理這類遺失物品的問題。":
                            "高速鉄道のサービスカウンターをご利用ください。スタッフが遺失物に関する対応をさせていただきます。",
                        
                        "請問高鐵服務櫃台怎麼走？我不是很熟悉這裡。":
                            "高速鉄道のサービスカウンターへの行き方を教えていただけますか？この辺りに詳しくないもので。",
                        
                        "沒問題，您從這裡直走，大約50公尺後左轉，您會看到高鐵的服務櫃台就在那邊。":
                            "もちろんです。ここからまっすぐ約50メートル進んで左に曲がっていただくと、高速鉄道のサービスカウンターがございます。"
                    },
                    "ja_to_zh": {
                        # 機場場景
                        "すみません、私の荷物が見当たらないのですが、ご協力いただけますか？":
                            "不好意思，我的東西不見了，您能幫我嗎？",
                        
                        "もちろんです。遺失物センターをお探しでしょうか？そちらのサービスをご案内できます。":
                            "當然可以，您是想尋找遺失物中心嗎？這裡有相關的服務。",
                        
                        "はい、どうすればよいでしょうか？":
                            "是的，我該怎麼做？",
                        
                        "空港インフォメーションカウンターをご利用ください。スタッフが遺失物に関する対応をさせていただき、必要なサポートをご提供いたします。":
                            "您可以前往機場服務台，工作人員會幫助您處理遺失物品的問題，並提供必要的協助。",
                        
                        "事前に問い合わせできる電話番号はありますか？":
                            "請問有電話可以聯絡的嗎？我希望能提前詢問一下。",
                        
                        "はい、ございます。航空警察局に直接お電話いただけます。電話番号は02-8770-2666です。遺失物についての確認をしていただけます。":
                            "當然可以，您可以直接打給航警局詢問，他們的電話是02-8770-2666。他們會協助查詢遺失的物品。",
                        
                        "ありがとうございます！早速連絡してみます。":
                            "非常感謝！我會馬上聯絡他們。",
                        
                        "どういたしまして！他にご質問やお困りのことがございましたら、いつでもお申し付けください！":
                            "不客氣！如果您有其他問題或需要幫助，隨時可以告訴我們喔！",

                        # 高鐵場景
                        "すみません、私の荷物が見当たらないのですが、どうすればよいでしょうか？":
                            "不好意思，我的東西不見了，請問該怎麼辦？",
                        
                        "こんにちは。遺失物センターで荷物をお探しするお手伝いをさせていただけますが、いかがでしょうか？":
                            "您好，您是否需要尋找遺失物中心來幫您尋回物品呢？",
                        
                        "はい、どこに行けばよいですか？":
                            "是的，我應該去哪裡找？",
                        
                        "高速鉄道のサービスカウンターをご利用ください。スタッフが遺失物に関する対応をさせていただきます。":
                            "您可以前往高鐵的服務櫃台，他們會幫助您處理這類遺失物品的問題。",
                        
                        "高速鉄道のサービスカウンターへの行き方を教えていただけますか？この辺りに詳しくないもので。":
                            "請問高鐵服務櫃台怎麼走？我不是很熟悉這裡。",
                        
                        "もちろんです。ここからまっすぐ約50メートル進んで左に曲がっていただくと、高速鉄道のサービスカウンターがございます。":
                            "沒問題，您從這裡直走，大約50公尺後左轉，您會看到高鐵的服務櫃台就在那邊。"
                    }
                }
            }

            # 保存腳本
            self._save_script(fun_pass_script)
            self._save_script(mrt_script)
            self._save_script(post_office_script)
            self._save_script(taxi_script)
            self._save_script(lost_found_script)
            
            # 載入所有腳本
            for filename in os.listdir(self.scripts_path):
                if filename.endswith('.json'):
                    with open(os.path.join(self.scripts_path, filename), 'r', encoding='utf-8') as f:
                        script = json.load(f)
                        self.scripts[script['name']] = script

        def get_translation_pair(self, text: str, source_lang: str, target_lang: str) -> Optional[str]:
            """
            找到匹配的文本及其對應的翻譯
            
            Parameters:
            -----------
            text : str
                要查找的原文
            source_lang : str
                源語言代碼
            target_lang : str
                目標語言代碼
            
            Returns:
            --------
            Optional[str]
                找到的對應翻譯，如果沒找到則返回 None
            """
            # 建構翻譯方向的鍵值
            translation_direction = f"{source_lang.lower()}_to_{target_lang.lower()}"
            
            # 遍歷所有腳本
            for script in self.scripts.values():
                translations = script.get("translations", {})
                translation_map = translations.get(translation_direction, {})
                
                # 嘗試直接匹配
                if text.strip() in translation_map:
                    return translation_map[text.strip()]
            
            return None

        def _save_script(self, script: dict):
            """保存腳本到文件"""
            filename = f"{script['name']}.json"
            with open(os.path.join(self.scripts_path, filename), 'w', encoding='utf-8') as f:
                json.dump(script, f, ensure_ascii=False, indent=2)

        def get_all_scripts(self, language: str) -> dict:
            """獲取指定語言的所有腳本內容"""
            all_texts = {}
            source_lang = language.lower()
            
            # 獲取所有文本內容
            for script in self.scripts.values():
                translations = script.get("translations", {})
                # 獲取所有文本（不論是源文本還是目標文本）
                for direction, trans_map in translations.items():
                    if direction.startswith(f"{source_lang}_to_") or direction.endswith(f"_to_{source_lang}"):
                        # 如果是 source_lang_to_X，取 key；如果是 X_to_source_lang，取 value
                        if direction.startswith(f"{source_lang}_to_"):
                            all_texts.update(trans_map)
                        else:
                            # 對於反向映射，交換 key 和 value
                            reversed_map = {v: k for k, v in trans_map.items()}
                            all_texts.update(reversed_map)
            
            return all_texts

    class FlexibleScriptMatcher:
        def __init__(self, script_db, similarity_threshold: float = 0.8):
            self.script_db = script_db
            self.similarity_threshold = similarity_threshold
            self.keywords_cache = {}
            self._update_keywords()

        def _update_keywords(self):
            """更新關鍵字緩存"""
            for language in ['zh', 'ja']:
                self.keywords_cache[language] = self._extract_keywords(language)

        def _extract_keywords(self, language: str) -> dict:
            """從腳本中提取關鍵字"""
            all_keywords = {
                "zh": {
                    "遺失物": ["遺失", "不見", "找回", "丟失", "遺失物中心", "遺失物", "處理"],
                    "計程車": ["計程車", "招呼站", "叫車", "等候", "應用程式", "手機叫車", "搭車", "過來"],
                    "郵局": ["郵局", "信件", "包裹", "掛號", "文件", "寄送", "貴重物品", "郵寄"],
                    "捷運": ["捷運", "捷運站", "紅線", "末班車", "台北101站", "入口", "站"],
                    "票卡": ["北北基好玩卡", "悠遊卡", "票", "卡別", "優惠", "FunPASS", "購買"],
                    "位置": ["櫃台", "門口", "右手邊", "外面", "走到", "服務台", "左手邊", "東三門", "南一門", "前方", "直走", "公尺", "附近"],
                    "時間": ["分鐘", "通常", "等待", "過來", "營業時間", "下午", "中午", "週一", "週五", "星期天", "凌晨", "末班"],
                    "服務": ["服務", "協助", "幫助", "辦理", "解答", "諮詢", "購買", "中心"],
                    "詢問": ["請問", "不好意思", "謝謝", "好的", "了解", "沒問題"]
                },
                "ja": {
                    "遺失物": ["遺失物", "荷物", "見当たらない", "探す", "センター", "対応", "確認"],
                    "計程車": ["タクシー", "乗り場", "配車", "アプリ", "待機", "スマートフォン", "乗車"],
                    "郵局": ["郵便局", "手紙", "荷物", "書留", "書類", "配達", "貴重品", "郵便"],
                    "捷運": ["地下鉄", "駅", "赤線", "最終電車", "台北101駅", "入口", "改札"],
                    "票卡": ["北北基好玩卡", "Taipei FunPASS", "チケット", "カード", "特典", "購入"],
                    "位置": ["カウンター", "出口", "右手", "外", "進む", "インフォメーション", "左手", "東三門", "南一門", "前方", "まっすぐ", "メートル", "付近"],
                    "時間": ["分", "通常", "待つ", "来ます", "営業時間", "午後", "正午", "月曜", "金曜", "日曜", "午前", "最終"],
                    "服務": ["サービス", "ご案内", "お手伝い", "手続き", "ご質問", "ご相談", "購入", "センター"],
                    "詢問": ["すみません", "ありがとうございます", "はい", "分かりました", "大丈夫です"]
                }
            }
            return all_keywords.get(language, {})

        def _normalize_text(self, text: str) -> str:
            """標準化文本"""
            text = re.sub(r'[^\w\s]', '', text)
            text = re.sub(r'\s+', ' ', text)
            return text.strip()

        def _calculate_similarity(self, text1: str, text2: str) -> float:
            """計算文本相似度"""
            return SequenceMatcher(None, self._normalize_text(text1), self._normalize_text(text2)).ratio()

        def _contains_keywords(self, text: str, keywords: set, threshold: int = 2) -> bool:
            """檢查關鍵字匹配度"""
            return sum(1 for keyword in keywords if keyword in text) >= threshold

        def find_matching_script(self, text: str, language: str) -> Tuple[Union[str, None], float]:
            """查找最匹配的腳本"""
            if language not in self.keywords_cache:
                return None, 0.0

            normalized_input = self._normalize_text(text)
            best_match = None
            best_score = 0.0

            # 從所有腳本中查找最佳匹配
            available_texts = self.script_db.get_all_scripts(language)
            logger.debug(f"可用的腳本數量: {len(available_texts)}")
            
            for script_text in available_texts.keys():  # 現在直接使用文本內容
                normalized_script = self._normalize_text(script_text)
                similarity = self._calculate_similarity(normalized_input, normalized_script)
                
                # 檢查關鍵字
                has_keywords = any(
                    self._contains_keywords(normalized_input, set(keywords))
                    for keywords in self.keywords_cache[language].values()
                )
                
                # logger.debug(f"文本匹配: '{script_text}' 相似度: {similarity:.2f} 包含關鍵字: {has_keywords}")

                # 更新最佳匹配
                if similarity > best_score and (similarity >= self.similarity_threshold or has_keywords):
                    best_score = similarity
                    best_match = script_text
                    logger.debug(f"找到更好的匹配: {script_text} (分數: {similarity:.2f})")

            if best_match:
                logger.info(f"最佳匹配結果 (分數: {best_score:.2f}): {best_match}")
            else:
                logger.info("未找到符合條件的匹配")

            return best_match, best_score
        

    def __init__(self, auth_key=None):
        """
        初始化 DeepLTranslatorService，使用提供的認證金鑰或設定中的認證金鑰。
        
        :param auth_key: str, 可選的認證金鑰，如果未提供，將從設定中讀取

        初始化過程:
        1. 設置 DeepL API 認證密鑰。
        2. 初始化 OpenCC 用於簡繁轉換。
        3. 創建 DeepL Translator 實例。
        4. 初始化空的詞彙表字典。
        5. 加載所有可用的詞彙表。

        使用範例
        translator_service = DeepLTranslatorService(log_level="DEBUG")
        translated_text = translator_service.translate("Hello, world!", "FR")
        """
        self.opencc = OpenCC('s2tw')  # Convert Simplified Chinese to Traditional Chinese (Taiwan)
        self.auth_key = auth_key or settings.DEEPL_AUTH_KEY_JSON["key"]
        self.send_platform_info = False
        if not self.auth_key:
            raise ValueError("必須提供認證金鑰")

        try:
            self.translator = deepl.Translator(
                self.auth_key,
                send_platform_info = self.send_platform_info
            )
            # 使用 DeepL 翻譯器實例初始化 GlossaryManager
            self.glossary_manager = GlossaryManager(self.translator)
            logger.info("DeepL 翻譯客戶端呼叫成功")
        except Exception as e:
            logger.error(f"呼叫 DeepL 翻譯客戶端失敗: {e}")
            raise

        # 初始化腳本匹配系統（現在使用內部類）
        self.script_db = self.ScriptDatabase()
        self.script_matcher = self.FlexibleScriptMatcher(
            self.script_db, 
            similarity_threshold=0.65
        )
        logger.info("腳本匹配系統初始化成功")

        self.glossaries: Dict[Tuple[str, str, str], str] = {}
        self._load_glossaries()
        self.DEFAULT_DOMAIN="18國多語言旅遊詞彙表-觀傳局-松山機場-台北車站"
    
    def _load_glossaries(self):
        """
        加載所有可用的詞彙表信息。

        這個方法在服務初始化時被調用，它會:
        1. 從 DeepL API 獲取所有可用的詞彙表。
        2. 解析每個詞彙表的名稱以提取 domain, source_lang, 和 target_lang。
        3. 將解析後的信息存儲在 self.glossaries 字典中。

        注意:
            - 假設詞彙表的命名格式為 "domain_source_target"。
            - 對於不符合預期格式的詞彙表名稱，會記錄警告並跳過。
        
        異常:
            - 如果在加載過程中發生 DeepL API 相關的錯誤，會記錄錯誤信息但不會中斷程序。
        """
        try:
            all_glossaries = self.translator.list_glossaries()
            for glossary in all_glossaries:
                # 假設詞彙表名稱格式為 "domain_source_target"
                parts = glossary.name.split('_')
                if len(parts) == 3:
                    domain, source_lang, target_lang = parts
                    key = (domain, source_lang, target_lang)
                    self.glossaries[key] = glossary.glossary_id
                    logger.info(f"加載詞彙表: {glossary.name} (ID: {glossary.glossary_id})")
                else:
                    logger.warning(f"跳過命名格式不符預期的詞彙表: {glossary.name}")
        except deepl.DeepLException as e:
            logger.error(f"加載詞彙表失敗: {e}")
    
    def get_glossary_id(self, domain: str, source_lang: str, target_lang: str) -> Optional[str]:
        """
        根據給定的域、源語言和目標語言獲取相應的詞彙表 ID。

        參數:
            domain (str): 詞彙表的領域。
            source_lang (str): 源語言代碼。
            target_lang (str): 目標語言代碼。

        返回:
            Optional[str]: 如果找到匹配的詞彙表，返回其 ID；否則返回 None。

        注意:
            這個方法直接從預加載的 self.glossaries 字典中查詢，不會觸發 API 調用。
        """
        if domain is None:
            domain = self.DEFAULT_DOMAIN
        
        glossary_id = self.glossaries.get((domain, source_lang, target_lang))
        if glossary_id is None and domain != self.DEFAULT_DOMAIN:
            # 如果找不到指定 domain 的詞彙表，嘗試使用默認 domain
            glossary_id = self.glossaries.get((self.DEFAULT_DOMAIN, source_lang, target_lang))
            if glossary_id:
                logger.info(f"使用默認詞彙表領域 '{self.DEFAULT_DOMAIN}'")
        
        return glossary_id

    def translate(self, text: str, target_language: str, source_language: str, domain: Optional[str] = None) -> str:
        """
        將給定文本從源語言翻譯為目標語言，可選擇使用特定領域的詞彙表。

        參數:
            text (str): 要翻譯的文本。
            target_language (str): 目標語言代碼。
            source_language (str): 源語言代碼。
            domain (Optional[str]): 可選的領域，用於選擇特定的詞彙表。

        返回:
            str: 翻譯後的文本。

        異常:
            ValueError: 如果沒有提供文本或目標語言。
            RuntimeError: 如果翻譯過程中發生 DeepL API 相關的錯誤。

        流程:
            1. 驗證輸入參數。
            2. 標準化語言代碼。
            3. 驗證語言支持。
            4. 如果提供了 domain，嘗試獲取相應的詞彙表 ID。
            5. 使用 DeepL API 進行翻譯。
            6. 如果目標語言是中文，將結果轉換為繁體中文。
            7. 返回翻譯後的文本。
        """
        if not text:
            logger.error("未提供要翻譯的文本")
            raise ValueError("未提供要翻譯的文本")
        if not target_language:
            logger.error("必須指定目標語言")
            raise ValueError("必須指定目標語言")
        
        # 標準化語言代碼
        source_language = self._normalize_language_code(source_language)
        target_language = self._normalize_language_code(target_language)

        # 倘若 'EN-US' 為 source_lang 的話，需要修改為 'EN'
        # @https://github.com/DeepLcom/deepl-python/issues/33
        if source_language == 'EN-US' or source_language == 'EN-GB':
            source_language = 'EN'

        # 驗證語言支援
        self._validate_language_support(source_language, "來源")
        self._validate_language_support(target_language, "目標")

        try:
            # 2. 嘗試相似度匹配
            source_lang = source_language.lower()
            target_lang = target_language.lower()
            similar_text, similarity = self.script_matcher.find_matching_script(text, source_lang)
            logger.info(f"相似度匹配結果 - 分數: {similarity:.2f}, 閾值: {self.script_matcher.similarity_threshold}")
            
            if similar_text and similarity >= self.script_matcher.similarity_threshold:
                logger.info(f"找到相似文本: {similar_text}")
                matched_translation = self.script_db.get_translation_pair(similar_text, source_lang, target_lang)
                
                if matched_translation:
                    logger.info("使用相似文本的翻譯")
                    return self._process_chinese_text(matched_translation, target_language)
                else:
                    logger.info("相似文本沒有對應翻譯")
                
            logger.info(f"正在將文本 '{text}' 從原始語言 '{source_language}', 翻譯為目標語言 '{target_language}'")
            # 如果指定了領域，則取得該領域的詞彙表 ID
            glossary_id = self.get_glossary_id(domain, source_language, target_language)
            
            if glossary_id:
                logger.info(f"使用詞彙表 ID: {glossary_id}")
            else:
                logger.info("未找到合適的詞彙表，將不使用詞彙表進行翻譯")

            # 2024/07/ DeepL 更新繁體中文在 Target_language
            # 如果目標語言是中文，則將簡體中文轉換為繁體中文
            if target_language == 'ZH':
                target_language = 'ZH-HANT'

            result = self.translator.translate_text(
                text, 
                target_lang=target_language, 
                source_lang=source_language,
                glossary=glossary_id
            )
            translated_text = result.text
            
            # 如果目標語言是中文，則將簡體中文轉換為繁體中文
            if target_language == 'ZH':
                translated_text = self.opencc.convert(translated_text)
            
            logger.debug(f"翻譯成功: {translated_text}")
            return translated_text
            
        except deepl.DeepLException as e:
            logger.error(f"翻譯文本失敗: {e}")
            raise RuntimeError(f"由於以下原因，翻譯文本失敗: {e}")
    
    def _process_chinese_text(self, text: str, target_language: str) -> str:
        """處理中文文本的輔助方法"""
        if target_language == 'ZH':
            return self.opencc.convert(text)
        return text

    def _normalize_language_code(self, language_code: str) -> str:
        """
        將語言代碼標準化為 DeepL API 預期的格式。

        :param language_code: 原始語言代碼
        :return: 標準化後的語言代碼
        """
        language_code = language_code.upper()
        if language_code == 'EN':
            return 'EN-US'
        if language_code == 'ZH_HANT':
            return 'ZH'
        return language_code

    def _validate_language_support(self, language_code: str, language_type: str) -> None:
        """
        驗證給定的語言代碼是否受支援。

        :param language_code: 要驗證的語言代碼
        :param language_type: 語言類型（來源或目標），用於錯誤訊息
        """
        if language_code not in self.SUPPORTED_LANGUAGES:
            error_message = f"{language_type}語言 {language_code} 不在支援的語言列表中"
            logger.warning(error_message)
            raise ValueError(error_message)
    
    def get_languages(self):
        """
        獲取當前可用的源語言和目標語言。

        :return: dict, 包含源語言和目標語言的信息
        """
        try:
            source_languages = self.translator.get_source_languages()
            target_languages = self.translator.get_target_languages()

            languages = {
                "source_languages": [{"name": lang.name, "code": lang.code} for lang in source_languages],
                "target_languages": [
                    {
                        "name": lang.name,
                        "code": lang.code,
                        "supports_formality": lang.supports_formality
                    } for lang in target_languages
                ]
            }

            return languages
        except deepl.DeepLException as e:
            logger.error(f"獲取語言失敗: {e}")
            raise RuntimeError(f"由於以下原因，獲取語言失敗: {e}")
    
    # 新增的詞彙表管理方法

    def create_glossary(self, name: str, source_lang: str, target_lang: str, entries: dict, domain: Optional[str] = None):
        """
        建立新的詞彙表，並可選擇將其與特定領域關聯。

        :param name: 詞彙表名稱
        :param source_lang: 來源語言代碼
        :param target_lang: 目標語言代碼
        :param entries: 詞彙表條目的字典 (來源詞: 目標詞)
        :param domain: 可選的關聯領域
        :return: 包含已建立詞彙表詳細資訊的 GlossaryInfo 物件
        """
        return self.glossary_manager.create_glossary(name, source_lang, target_lang, entries, domain)

    def get_glossary(self, glossary_id: str):
        """
        透過 ID 取得特定詞彙表。

        :param glossary_id: 要取得的詞彙表 ID
        :return: 包含所請求詞彙表詳細資訊的 GlossaryInfo 物件
        """
        return self.glossary_manager.get_glossary(glossary_id)

    def list_glossaries(self):
        """
        列舉與 DeepL 帳戶關聯的所有詞彙表。

        :return: GlossaryInfo 物件的列表
        """
        return self.glossary_manager.list_glossaries()

    def delete_glossary(self, glossary_id: str):
        """
        透過 ID 刪除特定詞彙表。

        :param glossary_id: 要刪除的詞彙表 ID
        """
        self.glossary_manager.delete_glossary(glossary_id)

    def get_glossary_entries(self, glossary_id: str):
        """
        取得特定詞彙表的條目。

        :param glossary_id: 要取得條目的詞彙表 ID
        :return: 詞彙表條目的字典 (來源詞: 目標詞)
        """
        return self.glossary_manager.get_glossary_entries(glossary_id)