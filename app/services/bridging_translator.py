from fastapi import Depends
from loguru import logger
from app.services.madlad400_translator import Madlad400TranslatorService
from app.core.config import settings

class BridgingTranslatorService:
    _instance = None

    def __new__(cls, *args, **kwargs):
        if cls._instance is None:
            cls._instance = super().__new__(cls)
        return cls._instance

    def __init__(self, 
                 custom_translator: Madlad400TranslatorService, 
                 bridge_lang: str = "en"
        ):
        """
        初始化 BridgingTranslatorService，設置中間語言。參考資料：https://machinetranslate.org/bridging

        :param custom_translator: CustomTranslatorService, 自研翻譯服務
        :param bridge_lang: str, 中間翻譯語言，默認為英文 "en"
        """
        self.custom_translator = custom_translator
        self.bridge_lang = bridge_lang

    def bridge_translate(self, input_text: str, target_language: str, source_language: str) -> str:
        """
        將原始文本先翻譯成中間語言，再從中間語言翻譯到目標語言。

        :param input_text: str, 要翻譯的文本
        :param target_language: str, 目標語言代碼
        :return: str, 最終翻譯結果
        """
        logger.info(f"[1/3] 將原始文本先翻譯成中間語言 {self.bridge_lang}，再從中間語言翻譯到目標語言：{target_language}")
        
        # 首先將文本翻譯成中間語言
        intermediate_translation = self.custom_translator.translate_text(input_text, self.bridge_lang, source_language)
        logger.info(f"[2/3] 中間語言翻譯結果：{intermediate_translation}")
        
        # 然後將中間語言翻譯成目標語言
        final_translation = self.custom_translator.translate_text(intermediate_translation, target_language, source_language)
        logger.info(f"[3/3] 最終翻譯結果：{final_translation}")
        
        return final_translation

def get_custom_translator_service() -> Madlad400TranslatorService:
    """
    獲取 CustomTranslatorService 實例。

    :return: CustomTranslatorService, 自研翻譯服務實例
    """
    return Madlad400TranslatorService()

def get_bridging_translator_service(custom_translator: Madlad400TranslatorService = Depends(get_custom_translator_service)) -> BridgingTranslatorService:
    """
    獲取 BridgingTranslatorService 實例。

    :param custom_translator: CustomTranslatorService, 自研翻譯服務
    :return: BridgingTranslatorService, Bridging 翻譯服務實例
    """
    return BridgingTranslatorService(custom_translator)