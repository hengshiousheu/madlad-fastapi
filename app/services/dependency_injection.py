from app.core.config import settings
from fastapi import Depends
from app.services.deepl_translator import DeepLTranslatorService
from app.services.google_translator import GoogleTranslateService
from app.services.madlad400_translator import Madlad400TranslatorService
from app.services.bridging_translator import BridgingTranslatorService

def get_deepl_translator_service() -> DeepLTranslatorService:
    """
    獲取 DeepL 翻譯服務實例。
    """
    return DeepLTranslatorService()

def get_google_translator_service() -> GoogleTranslateService:
    """
    獲取 Google 翻譯服務實例。
    """
    return GoogleTranslateService(project_id=settings.GOOGLE_APPLICATION_CREDENTIALS_JSON["project_id"])

def get_madlad400_translator_service() -> Madlad400TranslatorService:
    """
    獲取自研翻譯服務實例。
    """
    return Madlad400TranslatorService()

def get_bridging_translator_service(custom_translator: Madlad400TranslatorService = Depends(get_madlad400_translator_service)) -> BridgingTranslatorService:
    """
    獲取 Bridging 翻譯服務實例。
    """
    return BridgingTranslatorService(custom_translator)