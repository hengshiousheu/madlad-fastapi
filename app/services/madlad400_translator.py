import os
import ctranslate2
from sentencepiece import SentencePieceProcessor
from app.core.config import settings
from loguru import logger
from functools import lru_cache
import requests
from huggingface_hub import snapshot_download

class Madlad400TranslatorService:

    _instance = None

    def __new__(cls, *args, **kwargs):
        if cls._instance is None:
            cls._instance = super().__new__(cls)
        return cls._instance

    def __init__(self):
        """
        初始化自有模型翻譯服務，檢查並下載模型，並加載 tokenizer 和 translator。
        """
        os.environ["CT2_USE_EXPERIMENTAL_PACKED_GEMM"] = settings.CT2_USE_EXPERIMENTAL_PACKED_GEMM
        self.model_path = self.download_model(settings.MADLAD400_MODEL_NAME)
        self.tokenizer = SentencePieceProcessor()
        self.tokenizer.load(f"{self.model_path}/{settings.MADLAD400_SENTENCE_PIECE_NAME}")
        self.translator = ctranslate2.Translator(self.model_path, device=settings.DEVICE)
        logger.info(f"Madlad400 模型執行 DEVICE 結果:{settings.DEVICE}")

    def download_model(self, model_name: str, resume_download: bool = False, local_files_only: bool = False) -> str:
        """
        下載模型，如果模型已存在，則不重複下載。
        
        :param model_name: str, 模型名稱
        :param resume_download: bool, 是否繼續下載未完成的模型
        :param local_files_only: bool, 是否僅從本地加載模型
        :return: str, 模型路徑
        """
        kwargs = {
            "resume_download": resume_download,
            "local_files_only": local_files_only,
        }
        try:
            kwargs["resume_download"] = True
            model_path = snapshot_download(model_name, **kwargs)
            logger.info(f"模型路徑 {model_path} 模型 {model_name} 下載完成或從緩存中獲取。")
            return model_path
        except requests.exceptions.ConnectionError as exception:
            logger.warning(f"同步模型 {model_name} 從 Hugging Face Hub 時發生錯誤:\n{exception}, 嘗試直接從本地緩存加載模型（如果存在）。")
            kwargs["local_files_only"] = True
            return snapshot_download(model_name, **kwargs)

    def handle_newlines(self, input_text: str, target_language: str):
        """
        處理含有多個換行的文本。
        
        :param input_text: str, 輸入文本
        :param target_language: str, 目標語言代碼
        :return: list, 編碼後的句子列表
        """
        sentences = input_text.split('\n')
        sentences = [s.strip() for s in sentences if s.strip()]
        encoded_sentences = [
            self.tokenizer.encode(f"<2{target_language}> {sentence}", out_type=str) for sentence in sentences
        ]
        return encoded_sentences

    @lru_cache(maxsize=128)
    def translate_text(self, input_text: str, target_language: str, source_language: str) -> str:
        """
        使用自有模型進行翻譯。
        
        :param input_text: str, 輸入文本
        :param target_language: str, 目標語言代碼
        :return: str, 翻譯後的文本
        """
        logger.info(f"欲翻譯文字 {input_text} 目標語系 {target_language}")
        tokens = [self.tokenizer.decode(i) for i in range(460)]
        lang_codes = [token[2:-1] for token in tokens if token.startswith("<2")]
        if target_language not in lang_codes:
            raise ValueError("Unsupported language code.")
        
        encoded_sentences = self.handle_newlines(input_text, target_language)
        logger.debug(f"tokenizer 結果:{encoded_sentences}")

        results = self.translator.translate_batch(
            encoded_sentences, 
            batch_type="tokens", 
            beam_size=4, 
            sampling_topk=10,
            sampling_temperature=0.7,
            no_repeat_ngram_size=1,
            max_decoding_length=64,
            return_end_token=True,
            repetition_penalty=1.5
        )
        logger.debug(f"results 結果:{results}")

        translated_sentences = []
        for result in results:
            for hypothesis in result.hypotheses:
                sentence = self.tokenizer.decode(hypothesis)
                sentence = sentence.strip()
                translated_sentences.append(sentence)

        combined_sentence = ''.join(translated_sentences)
        logger.info(f"翻譯結果 {combined_sentence}")
        return combined_sentence
