from pydantic import BaseModel, ValidationError, validator, Field
from enum import Enum

class Language(str, Enum):
    zh = 'zh'        # 中文
    zh_Hant = 'zh_Hant'# 繁體中文
    en = 'en'        # 英文
    ja = 'ja'        # 日文
    ko = 'ko'        # 韩文
    id = 'id'        # 印尼文
    vi = 'vi'        # 越南文
    th = 'th'        # 泰文
    ms = 'ms'        # 马来西亚语
    es = 'es'        # 西班牙语
    it = 'it'        # 意大利语
    de = 'de'        # 德语
    nl = 'nl'        # 荷兰语
    pt = 'pt'        # 葡萄牙语
    fr = 'fr'        # 法语
    pl = 'pl'        # 波兰语
    ru = 'ru'        # 俄语
    tr = 'tr'        # 土耳其语
    sv = 'sv'        # 瑞典语

    @property
    def full_name(self):
        language_full_names = {
            'zh': 'Chinese',
            'zh_Hant': 'Traditional Chinese',
            'en': 'English',
            'ja': 'Japanese',
            'ko': 'Korean',
            'id': 'Indonesian',
            'vi': 'Vietnamese',
            'th': 'Thai',
            'ms': 'Malay',
            'es': 'Spanish',
            'it': 'Italian',
            'de': 'German',
            'nl': 'Dutch',
            'pt': 'Portuguese',
            'fr': 'French',
            'pl': 'Polish',
            'ru': 'Russian',
            'tr': 'Turkish',
            'sv': 'Swedish',
        }
        return language_full_names[self.value]
    # 提供枚举值的描述，虽然这不会直接显示在Swagger UI的枚举下拉列表中，但可以在API描述中提及

class ZeroShotQueryTranslationRequest(BaseModel):
    query: str = Field(..., example="Hello world")
    source_language: Language = Field(..., description="Source language of the text to be translated.")
    target_language: Language = Field(..., description="Target language for the translation.")

    @validator('source_language', 'target_language', pre=True)
    def validate_language(cls, value):
        if isinstance(value, str):
            value = value.lower()  # 將輸入值轉換為小寫
            if value == 'zh_hant':
                value = 'zh_Hant'
        try:
            return Language(value)
        except ValueError:
            raise ValueError(f"{value} is not a supported language code")
    class Config:
        json_schema_extra = {
            "example": {
                "query": "廁所的話 前面右轉直走到底就到了",
                "source_language": "zh",
                "target_language": "ja",
            }
        }

class ZeroShotQueryTranslationResponse(BaseModel):
    source_sentence: str = Field(..., example="請問廁所在哪裡", description="原始的要翻译的文本。")
    source_language: str = Field(..., example="zh", description="原始文本的語言代碼。")
    target_language: str = Field(..., example="ja", description="目标语言的代码。支持的语言代码包括 'en', 'zh', 'zh_Hant', 'ja', 'ko', 'vi', 'id', 'th', 'ms', 'es', 'it', 'de', 'nl', 'pt', 'fr', 'pl', 'ru', 'tr', 'sv' 等。")
    target_sentence: str = Field(..., example="トイレはどこですか", description="翻译后的文本。")
    execution_time: float = Field(..., description="zero-shot查詢翻譯執行時間（以秒為單位）。", round_decimal=6)