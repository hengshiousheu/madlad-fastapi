import time
from typing import Optional
from dataclasses import dataclass
from enum import Enum

class TermType(str, Enum):
    """術語類型定義"""
    TECHNICAL = "technical"  # 技術術語
    GENERAL = "general"      # 一般用語
    INDUSTRY = "industry"    # 行業專用詞
    DAILY = "daily" # 日長
    TRAVEL = "travel" #旅遊
    FOOD = "food" #食物
    INTERNET = "internet" #網路用語
    PLACE = "place" #地名
    PERSON = "person" #人名用語
    TPE = "Taiwan_Taoyuan_International_Airport" #桃園機場專用

@dataclass
class Term:
    """術語資料結構"""
    cn_term: str
    tw_term: str
    term_type: TermType
    notes: Optional[str] = None
    created_at: float = time.time()