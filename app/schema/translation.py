from pydantic import BaseModel, Field
from typing import List, Dict, Optional

class TranslationRequest(BaseModel):
    input_text: str = Field(..., example="請問廁所在哪裡", description="要翻译的文本。")
    target_language: str = Field(..., example="ja", description="目标语言的代码。支持的语言代码包括 'en', 'zh', 'zh_Hant', 'ja', 'ko', 'vi', 'id', 'th', 'ms', 'es', 'it', 'de', 'nl', 'pt', 'fr', 'pl', 'ru', 'tr', 'sv' 等。")
    source_language: Optional[str] = Field(None, example="zh", description="源语言的代码。支持的语言代码与目标语言相同。如果未提供，则系统将自动检测源语言。")

class TranslationResponse(BaseModel):
    translated_text: str
    execution_time: float = Field(..., description="翻譯執行時間（以秒為單位）。", round_decimal=6)

class BatchTranslationRequest(BaseModel):
    input_text: str = Field(..., example="請問廁所在哪裡", description="要翻译的文本。")
    source_language: Optional[str] = Field(None, example="zh", description="源语言的代码。如果未提供，则会自动检测源语言。")
    target_languages: List[str] = Field(..., example=["ja", "en"], description="目标语言的代码列表。支持的语言代码包括 'en', 'zh', 'zh_Hant', 'ja', 'ko', 'vi', 'id', 'th' 等。")

class BatchTranslationResponse(BaseModel):
    source_sentence: str = Field(..., example="請問廁所在哪裡", description="原始的要翻译的文本。")
    source_language: str = Field(..., example="zh", description="原始文本的語言代碼。")
    target_languages: List[str] = Field(..., example=["ja", "en"], description="目标语言的代码列表。支持的语言代码包括 'en', 'zh', 'zh_Hant', 'ja', 'ko', 'vi', 'id', 'th' 等。")
    target_sentences: Dict[str, str] = Field(..., description="包含所有目标语言及其对应翻译结果的字典。")
    translated_text: Dict[str, str] = Field(..., description="包含所有目标语言及其对应翻译结果的字典。")
    execution_time: float = Field(..., description="翻譯執行時間（以秒為單位）。", round_decimal=6)