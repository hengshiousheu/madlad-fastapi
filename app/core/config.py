from pydantic_settings import BaseSettings
from datetime import datetime
import json 
from typing import Dict
from pathlib import Path

THIS_DIR = Path(__file__).parent

class Settings(BaseSettings):
    class Config:
        # 通过 Pydantic config 来指示模型应如何解析和加载环境变量
        env_file = Path(THIS_DIR, ".env"), #/madlad-fastapi/app/core
        env_file_encoding = 'utf-8'
        json_encoders = {
            dict: json.dumps  # 确保字典类型可以正确编码为 JSON 字符串
        }
    
    PROJECT_NAME: str = "Madlad400-FastAPI"
    PROJECT_DESCRIPTION: str = "提供 Madlad400 系列模型进行翻译"
    PROJECT_VERSION: str = "1.0.0"
    BUILD_DATE: str = datetime.now().strftime("%Y-%m-%d %H:%M:%S %Z")
    CONTACT: dict = {
        "name": "Heng-Shiou Sheu",
        "url": "https://huggingface.co/Heng666",
    }
    LICENSE_INFO: dict = {
        "name": "使用 MIT 许可证",
        "url": "https://opensource.org/licenses/MIT",
    }
    MADLAD400_MODEL_NAME: str = "Heng666/madlad400-3b-mt-ct2-int8"
    MADLAD400_SENTENCE_PIECE_NAME: str = "spiece.model"
    DEVICE: str = "auto"
    CT2_USE_EXPERIMENTAL_PACKED_GEMM: str = "1"

    MACHINE_TRANSLATION_STRATEGY: str = "" # deepl, google, madlad400, bridging, default

    # 新增环境变量字段
    GOOGLE_APPLICATION_CREDENTIALS_JSON: Dict[str, str] = {
        "type": "service_account",
        "project_id": "PROVIED_PLEASE",
        "private_key_id": "PROVIDE_KEY_ID",
        "private_key": "PRIVATE_KEY",
        "client_email":"CLIENT_EMAIL",
        "client_id":"CLIENT_ID",
        "auth_uri":"https://accounts.google.com/o/oauth2/auth",
        "token_uri":"https://oauth2.googleapis.com/token",
        "auth_provider_x509_cert_url":"https://www.googleapis.com/oauth2/v1/certs",
        "client_x509_cert_url":"CLIENT_X509_CERT_URL",
        "universe_domain":"googleapis.com"
    }

    DEEPL_AUTH_KEY_JSON: Dict[str, str] = {
        "key": "DEEPL_KEY_FROM_WEBISTE"
    }

settings = Settings()
