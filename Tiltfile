# Tiltfile
# 維護人員：許恆修
# 註解日期：2024/07/15

# version_settings() 確保 Tilt 的最低版本要求
# https://docs.tilt.dev/api.html#api.version_settings
version_settings(constraint='>=0.22.2')

# docker_build 用於構建 Docker 映像，並配置實時更新以同步代碼變更
# https://docs.tilt.dev/api.html#api.docker_build
# https://docs.tilt.dev/live_update_reference.html
docker_build(
    'xiuxiumycena/madlad-fastapi:gpu-0.0.2',
    context='.',
    dockerfile='./Dockerfile.gpu',
    build_args={
        'PLATFORM': 'linux/amd64'
    },
    only=[
        './Dockerfile.gpu',
        './app/',
        './requirements.txt'
    ],
    live_update=[
        # 當源代碼文件發生變更時，將其同步到開發服務器的正確位置
        sync('./app/', '/app/app/'),
        # 當 requirements.txt 發生變更時，運行 pip 來更新依賴項
        run(
            'pip install -r /app/requirements.txt',
            trigger=['./requirements.txt']
        )
    ],
    cache_from=['xiuxiumycena/madlad-fastapi:gpu-0.0.3']
)


# 使用 Kubernetes YAML 文件部署 InferenceService 資源
# Kubernetes YAML for the InferenceService
k8s_yaml([
    'mt-gpu-deployment.yaml',
    'mt-gpu-service.yaml',
    'huggingface-cache-persistentvolumeclaim.yaml'
    ])

# 自定義 Kubernetes 資源以便 Tilt 進行管理
# https://docs.tilt.dev/api.html#api.k8s_resource
k8s_resource(
    'tilt-madlad-gpu',
    port_forwards=['8002:8000'],  # 將本地端口5734轉發到容器內的端口8080
    labels=['tilt-madlad-gpu'],
)

# 在 Tilt UI 日誌中顯示歡迎訊息和開發指南
tiltfile_path = config.main_path

if config.tilt_subcommand == 'up':
    print("""
    歡迎來到 Madlad400-FastAPI 的開發環境！
    
    這個 Tiltfile 是為了簡化 Madlad400-FastAPI 項目的開發過程而設計的。
    
    **專案概述：**
    Madlad400-FastAPI 提供使用 Madlad400 系列模型的翻譯服務。該專案基於 FastAPI 框架構建，通過 REST API 支持快速且高效的文本翻譯。
    
    **特性：**
    - **快速翻譯：** 支持多種語言的快速翻譯，同時確保翻譯質量和速度。
    - **自動設備選擇：** 根據運行環境自動選擇最佳設備（CPU/GPU）來執行翻譯任務。
    - **易於部署：** 支持 Docker 容器化部署，簡化部署流程。

    **使用方法：**
    1. **開始開發：**
       運行 `tilt up` 開始開發環境。Tilt 將構建 Docker 映像並將應用部署到您的 Kubernetes 集群。
    
    2. **實時更新：**
       對 `./app/` 目錄中的源代碼或 `requirements.txt` 文件的任何更改將自動同步到運行中的容器，並重新安裝依賴項。
    
    3. **Kubernetes 資源：**
       Tiltfile 使用 `mt-gpu-deployment.yaml` 和 `mt-gpu-service.yaml` 來定義所需的 Kubernetes 資源。
    
    4. **端口轉發：**
       可以通過 `localhost:8009` 訪問應用程序，該端口轉發到容器內的 `8000` 端口。

    欲了解更多詳細資訊，請參閱專案的 README.md 文件。
    
    Tiltfile 位置：{tiltfile}
    """.format(tiltfile=tiltfile_path))