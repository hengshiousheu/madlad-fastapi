# tests/test_main.py
import pytest
from fastapi.testclient import TestClient
from app.main import app

# 使用 pytest 的 fixture 來設置測試客戶端
@pytest.fixture(scope="module")
def client():
    """
    設置一個測試客戶端。

    範圍設置為 "module" 代表此客戶端在整個模塊內重複使用。
    """
    with TestClient(app, base_url="http://testserver") as c:
        yield c

def test_app_startup(client):
    """
    測試應用是否成功啟動並且根路徑是否可訪問。
    
    此測試確認：
    1. 應用返回 200 狀態碼。
    """
    response = client.get("/")
    assert response.status_code == 200, "應用未成功啟動，狀態碼非 200"