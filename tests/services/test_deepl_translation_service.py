# tests/services/test_deepl_translator_service.py

import pytest
from unittest.mock import MagicMock, patch
from app.services.deepl_translator import DeepLTranslatorService
import deepl

# 初始化 DeepLTranslatorService 並模擬認證金鑰
@pytest.fixture
def translator_service():
    return DeepLTranslatorService(auth_key="dummy_key")

# 正常情況測試
def test_translate_normal_case(translator_service):
    translator_service.translator.translate_text = MagicMock(return_value=MagicMock(text="Bonjour, le monde !"))
    result = translator_service.translate("Hello, world!", "FR", "EN")
    assert result == "Bonjour, le monde !"
