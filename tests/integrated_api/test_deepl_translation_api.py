# tests/api/test_deepl_translation.py

import pytest
from fastapi.testclient import TestClient
from app.main import app
from app.api.deepl_translation import get_translate_service
from unittest.mock import MagicMock
from app.services.deepl_translator import DeepLTranslatorService
import deepl

# 創建 FastAPI 測試客戶端
client = TestClient(app)

## 測試 中文轉各種語系
@pytest.mark.parametrize("input_text, target_language, expected_translation, source_language", [
    ("今天溫度是多少啊?", "id", "Berapa suhu hari ini?", "zh_Hant") # 中文 -> 印尼文
])
def test_translate_from_zh_hant_to_id(input_text, target_language, expected_translation, source_language):
    """
    测试 /translate_deepl/ API 的不同语言翻译情况。
    应返回 200 状态码及相应的翻译文本。
    """

    request_payload = {
        "input_text": input_text,
        "target_language": target_language,
        "source_language": source_language
    }
    response = client.post("/api/deepl_translation/translate_deepl/", json=request_payload)
    assert response.status_code == 200, "应返回 200 状态码"
    assert response.json() == {"translated_text": expected_translation}, f"翻译结果不正确 for {target_language}"

@pytest.mark.parametrize("input_text, target_language, expected_translation, source_language", [
    ("今天溫度是多少啊?", "vi", "Nhiệt độ hôm nay là bao nhiêu?", "zh_Hant") # 中文 -> 越南文
])
def test_translate_from_zh_hant_to_vi(input_text, target_language, expected_translation, source_language):
    """
    测试 /translate_deepl/ API 的不同语言翻译情况。
    应返回 200 状态码及相应的翻译文本。
    """

    request_payload = {
        "input_text": input_text,
        "target_language": target_language,
        "source_language": source_language
    }
    response = client.post("/api/deepl_translation/translate_deepl/", json=request_payload)
    assert response.status_code == 400, "应返回 400 状态码"

@pytest.mark.parametrize("input_text, target_language, expected_translation, source_language", [
    ("今天溫度是多少啊?", "th", "วันนี้อุนนภูมิเท่าไหร่?", "zh_Hant") # 中文 -> 泰文
])
def test_translate_from_zh_hant_to_th(input_text, target_language, expected_translation, source_language):
    """
    测试 /translate_deepl/ API 的不同语言翻译情况。
    应返回 200 状态码及相应的翻译文本。
    """

    request_payload = {
        "input_text": input_text,
        "target_language": target_language,
        "source_language": source_language
    }
    response = client.post("/api/deepl_translation/translate_deepl/", json=request_payload)
    assert response.status_code == 400, "应返回 400 状态码"
    # assert response.json() == {"translated_text": expected_translation}, f"翻译结果不正确 for {target_language}"

@pytest.mark.parametrize("input_text, target_language, expected_translation, source_language", [
    ("請問廁所在哪裡呢？", "en", "Where is the restroom?", "zh_Hant") # 中文 -> 英文
])
def test_translate_from_zh_hant_to_en(input_text, target_language, expected_translation, source_language):
    """
    测试 /translate_deepl/ API 的不同语言翻译情况。
    应返回 200 状态码及相应的翻译文本。
    """

    request_payload = {
        "input_text": input_text,
        "target_language": target_language,
        "source_language": source_language
    }
    response = client.post("/api/deepl_translation/translate_deepl/", json=request_payload)
    assert response.status_code == 200, "应返回 200 状态码"
    assert response.json() == {"translated_text": expected_translation}, f"翻译结果不正确 for {target_language}"

@pytest.mark.parametrize("input_text, target_language, expected_translation, source_language", [
    ("請問廁所在哪裡呢？", "ko", "화장실은 어디에 있나요?", "zh_Hant") # 中文 -> 韓文
])
def test_translate_from_zh_hant_to_ko(input_text, target_language, expected_translation, source_language):
    """
    测试 /translate_deepl/ API 的不同语言翻译情况。
    应返回 200 状态码及相应的翻译文本。
    """

    request_payload = {
        "input_text": input_text,
        "target_language": target_language,
        "source_language": source_language
    }
    response = client.post("/api/deepl_translation/translate_deepl/", json=request_payload)
    assert response.status_code == 200, "应返回 200 状态码"
    assert response.json() == {"translated_text": expected_translation}, f"翻译结果不正确 for {target_language}"

@pytest.mark.parametrize("input_text, target_language, expected_translation, source_language", [
    ("請問廁所在哪裡呢？", "ja", "トイレはどこですか？", "zh_Hant") # 中文 -> 日文
])
def test_translate_from_zh_hant_to_ja(input_text, target_language, expected_translation, source_language):
    """
    测试 /translate_deepl/ API 的不同语言翻译情况。
    应返回 200 状态码及相应的翻译文本。
    """

    request_payload = {
        "input_text": input_text,
        "target_language": target_language,
        "source_language": source_language
    }
    response = client.post("/api/deepl_translation/translate_deepl/", json=request_payload)
    assert response.status_code == 200, "应返回 200 状态码"
    assert response.json() == {"translated_text": expected_translation}, f"翻译结果不正确 for {target_language}"

@pytest.mark.parametrize("input_text, target_language, expected_translation, source_language", [
    ("請問廁所在哪裡呢？", "ja", "トイレはどこですか？", "zh") # 中文 -> 日文
])
def test_translate_from_zh_to_ja(input_text, target_language, expected_translation, source_language):
    """
    测试 /translate_deepl/ API 的不同语言翻译情况。
    应返回 200 状态码及相应的翻译文本。
    """

    request_payload = {
        "input_text": input_text,
        "target_language": target_language,
        "source_language": source_language
    }
    response = client.post("/api/deepl_translation/translate_deepl/", json=request_payload)
    assert response.status_code == 200, "应返回 200 状态码"
    assert response.json() == {"translated_text": expected_translation}, f"翻译结果不正确 for {target_language}"


## 測試 各種語系轉中文
@pytest.mark.parametrize("input_text, target_language, expected_translation, source_language", [
    ("トイレはどこですか？", "zh_Hant", "廁所在哪裡？", "ja") # 日文 -> 中文
])
def test_translate_from_ja_to_zh_hant(input_text, target_language, expected_translation, source_language):
    """
    测试 /translate_deepl/ API 的不同语言翻译情况。
    应返回 200 状态码及相应的翻译文本。
    """

    request_payload = {
        "input_text": input_text,
        "target_language": target_language,
        "source_language": source_language
    }
    response = client.post("/api/deepl_translation/translate_deepl/", json=request_payload)
    assert response.status_code == 200, "应返回 200 状态码"
    assert response.json() == {"translated_text": expected_translation}, f"翻译结果不正确 for {target_language}"

@pytest.mark.parametrize("input_text, target_language, expected_translation, source_language", [
    ("トイレはどこですか？", "zh_Hant", "廁所在哪裡？", "ja") # 日文 -> 中文
])
def test_translate_from_ko_to_zh_hant(input_text, target_language, expected_translation, source_language):
    """
    测试 /translate_deepl/ API 的不同语言翻译情况。
    应返回 200 状态码及相应的翻译文本。
    """

    request_payload = {
        "input_text": input_text,
        "target_language": target_language,
        "source_language": source_language
    }
    response = client.post("/api/deepl_translation/translate_deepl/", json=request_payload)
    assert response.status_code == 200, "应返回 200 状态码"
    assert response.json() == {"translated_text": expected_translation}, f"翻译结果不正确 for {target_language}"

@pytest.mark.parametrize("input_text, target_language, expected_translation, source_language", [
    ("Where is the restroom?", "zh_Hant", "洗手間在哪裡？", "en") # 中文 -> 英文
])
def test_translate_from_en_to_zh_hant(input_text, target_language, expected_translation, source_language):
    """
    测试 /translate_deepl/ API 的不同语言翻译情况。
    应返回 200 状态码及相应的翻译文本。
    """

    request_payload = {
        "input_text": input_text,
        "target_language": target_language,
        "source_language": source_language
    }
    response = client.post("/api/deepl_translation/translate_deepl/", json=request_payload)
    assert response.status_code == 200, "应返回 200 状态码"
    assert response.json() == {"translated_text": expected_translation}, f"翻译结果不正确 for {target_language}"

@pytest.mark.parametrize("input_text, target_language, expected_translation, source_language", [
    ("วันนี้อุนนภูมิเท่าไหร่?", "zh_Hant", "今天溫度是多少啊?", "th") # 中文 -> 泰文
])
def test_translate_from_th_to_zh_hant(input_text, target_language, expected_translation, source_language):
    """
    测试 /translate_deepl/ API 的不同语言翻译情况。
    应返回 200 状态码及相应的翻译文本。
    """

    request_payload = {
        "input_text": input_text,
        "target_language": target_language,
        "source_language": source_language
    }
    response = client.post("/api/deepl_translation/translate_deepl/", json=request_payload)
    assert response.status_code == 400, "应返回 400 状态码"
    # assert response.json() == {"translated_text": expected_translation}, f"翻译结果不正确 for {target_language}"

@pytest.mark.parametrize("input_text, target_language, expected_translation, source_language", [
    ("Nhiệt độ hôm nay là bao nhiêu?", "zh_Hant", "今天溫度是多少啊?", "vi") # 中文 -> 越南文
])
def test_translate_from_vi_to_zh_hant(input_text, target_language, expected_translation, source_language):
    """
    测试 /translate_deepl/ API 的不同语言翻译情况。
    应返回 200 状态码及相应的翻译文本。
    """

    request_payload = {
        "input_text": input_text,
        "target_language": target_language,
        "source_language": source_language
    }
    response = client.post("/api/deepl_translation/translate_deepl/", json=request_payload)
    assert response.status_code == 400, "应返回 400 状态码"

@pytest.mark.parametrize("input_text, target_language, expected_translation, source_language", [
    ("Berapa suhu hari ini?", "zh_Hant", "今天的氣溫是多少？", "id") # 中文 -> 印尼文
])
def test_translate_from_id_to_zh_hant(input_text, target_language, expected_translation, source_language):
    """
    测试 /translate_deepl/ API 的不同语言翻译情况。
    应返回 200 状态码及相应的翻译文本。
    """

    request_payload = {
        "input_text": input_text,
        "target_language": target_language,
        "source_language": source_language
    }
    response = client.post("/api/deepl_translation/translate_deepl/", json=request_payload)
    assert response.status_code == 200, "应返回 200 状态码"
    assert response.json() == {"translated_text": expected_translation}, f"翻译结果不正确 for {target_language}"


def test_get_languages_endpoint():
    """
    測試 /available_languages/ API 的正常情況。
    應返回 200 狀態碼及可用的語言列表。
    """
    response = client.get("/api/deepl_translation/available_languages/")
    assert response.status_code == 200, "應返回 200 狀態碼"
    assert response.json() == {'source_languages': [{'name': 'Bulgarian', 'code': 'BG'}, {'name': 'Czech', 'code': 'CS'}, {'name': 'Danish', 'code': 'DA'}, {'name': 'German', 'code': 'DE'}, {'name': 'Greek', 'code': 'EL'}, {'name': 'English', 'code': 'EN'}, {'name': 'Spanish', 'code': 'ES'}, {'name': 'Estonian', 'code': 'ET'}, {'name': 'Finnish', 'code': 'FI'}, {'name': 'French', 'code': 'FR'}, {'name': 'Hungarian', 'code': 'HU'}, {'name': 'Indonesian', 'code': 'ID'}, {'name': 'Italian', 'code': 'IT'}, {'name': 'Japanese', 'code': 'JA'}, {'name': 'Korean', 'code': 'KO'}, {'name': 'Lithuanian', 'code': 'LT'}, {'name': 'Latvian', 'code': 'LV'}, {'name': 'Norwegian', 'code': 'NB'}, {'name': 'Dutch', 'code': 'NL'}, {'name': 'Polish', 'code': 'PL'}, {'name': 'Portuguese', 'code': 'PT'}, {'name': 'Romanian', 'code': 'RO'}, {'name': 'Russian', 'code': 'RU'}, {'name': 'Slovak', 'code': 'SK'}, {'name': 'Slovenian', 'code': 'SL'}, {'name': 'Swedish', 'code': 'SV'}, {'name': 'Turkish', 'code': 'TR'}, {'name': 'Ukrainian', 'code': 'UK'}, {'name': 'Chinese', 'code': 'ZH'}], 'target_languages': [{'name': 'Bulgarian', 'code': 'BG', 'supports_formality': False}, {'name': 'Czech', 'code': 'CS', 'supports_formality': False}, {'name': 'Danish', 'code': 'DA', 'supports_formality': False}, {'name': 'German', 'code': 'DE', 'supports_formality': True}, {'name': 'Greek', 'code': 'EL', 'supports_formality': False}, {'name': 'English (British)', 'code': 'EN-GB', 'supports_formality': False}, {'name': 'English (American)', 'code': 'EN-US', 'supports_formality': False}, {'name': 'Spanish', 'code': 'ES', 'supports_formality': True}, {'name': 'Estonian', 'code': 'ET', 'supports_formality': False}, {'name': 'Finnish', 'code': 'FI', 'supports_formality': False}, {'name': 'French', 'code': 'FR', 'supports_formality': True}, {'name': 'Hungarian', 'code': 'HU', 'supports_formality': False}, {'name': 'Indonesian', 'code': 'ID', 'supports_formality': False}, {'name': 'Italian', 'code': 'IT', 'supports_formality': True}, {'name': 'Japanese', 'code': 'JA', 'supports_formality': True}, {'name': 'Korean', 'code': 'KO', 'supports_formality': False}, {'name': 'Lithuanian', 'code': 'LT', 'supports_formality': False}, {'name': 'Latvian', 'code': 'LV', 'supports_formality': False}, {'name': 'Norwegian', 'code': 'NB', 'supports_formality': False}, {'name': 'Dutch', 'code': 'NL', 'supports_formality': True}, {'name': 'Polish', 'code': 'PL', 'supports_formality': True}, {'name': 'Portuguese (Brazilian)', 'code': 'PT-BR', 'supports_formality': True}, {'name': 'Portuguese (European)', 'code': 'PT-PT', 'supports_formality': True}, {'name': 'Romanian', 'code': 'RO', 'supports_formality': False}, {'name': 'Russian', 'code': 'RU', 'supports_formality': True}, {'name': 'Slovak', 'code': 'SK', 'supports_formality': False}, {'name': 'Slovenian', 'code': 'SL', 'supports_formality': False}, {'name': 'Swedish', 'code': 'SV', 'supports_formality': False}, {'name': 'Turkish', 'code': 'TR', 'supports_formality': False}, {'name': 'Ukrainian', 'code': 'UK', 'supports_formality': False}, {'name': 'Chinese (simplified)', 'code': 'ZH', 'supports_formality': False}]}, "語言列表結果不正確"
