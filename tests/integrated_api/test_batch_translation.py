import pytest
from fastapi.testclient import TestClient
from app.main import app
from app.schema.translation import BatchTranslationRequest
from app.strategy.enum import TranslationStrategyEnum

client = TestClient(app)

def test_translate_multiple_languages_normal_case():
    """
    測試 /batch-translate/ API 的正常情況。
    應返回 200 狀態碼及正確的翻譯結果。
    """
    request_payload = {
        "input_text": "請問廁所在哪裡",
        "source_language": "zh",
        "target_languages": ["ja", "en"]
    }
    expected_response = {
        "source_sentence": "請問廁所在哪裡",
        "source_language": "zh_Hant",
        "target_languages": ["ja", "en"],
        "target_sentences": {
            "ja": "トイレはどこにありますか？",
            "en": "I would like to know where the toilet is."
        },
        "translated_text": {
            "ja": "トイレはどこにありますか？",
            "en": "I would like to know where the toilet is."
        }
    }
    response = client.post("/v1/batch-translate/?strategy=madlad400", json=request_payload)
    assert response.status_code == 200, "應返回 200 狀態碼"
    assert response.json()['source_sentence'] == expected_response["source_sentence"], "返回 schema 不正確"
    assert response.json()['source_language'] == expected_response["source_language"], "返回的翻譯結果不正確"