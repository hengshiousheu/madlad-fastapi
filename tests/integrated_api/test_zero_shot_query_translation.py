# tests/api/test_zero_shot_query_translation.py

import pytest
from fastapi.testclient import TestClient
from unittest.mock import MagicMock, patch
from app.main import app
from app.schema.zero_shot_query import ZeroShotQueryTranslationRequest, ZeroShotQueryTranslationResponse
from app.strategy.strategies import TranslationStrategyFactory, TranslationStrategy
from app.strategy.enum import TranslationStrategyEnum

client = TestClient(app)

@pytest.mark.parametrize("source_language, target_language, expected_target_sentence", [
    # 測試從繁體中文翻譯到其他語言
    ("zh_Hant", "en", "翻譯後的文本"),  # 繁體中文 -> 英文
    ("zh_Hant", "ja", "翻譯後的文本"),  # 繁體中文 -> 日文
    ("zh_Hant", "ko", "翻譯後的文本"),  # 繁體中文 -> 韓文
    ("zh_Hant", "id", "翻譯後的文本"),  # 繁體中文 -> 印尼文
    ("zh_Hant", "vi", "翻譯後的文本"),  # 繁體中文 -> 越南文
    ("zh_Hant", "th", "翻譯後的文本"),  # 繁體中文 -> 泰文

    # 測試從其他語言翻譯到繁體中文
    ("en", "zh_Hant", "翻譯後的文本"),  # 英文 -> 繁體中文
    ("ja", "zh_Hant", "翻譯後的文本"),  # 日文 -> 繁體中文
    ("ko", "zh_Hant", "翻譯後的文本"),  # 韓文 -> 繁體中文
    ("id", "zh_Hant", "翻譯後的文本"),  # 印尼文 -> 繁體中文
    ("vi", "zh_Hant", "翻譯後的文本"),  # 越南文 -> 繁體中文
    ("th", "zh_Hant", "翻譯後的文本"),  # 泰文 -> 繁體中文
])
def test_zero_shot_query_multiple_languages(source_language, target_language, expected_target_sentence):
    """
    測試 /zero-shot-query API 多語言情況。
    確認不同語言組合的翻譯是否正確，並檢查 API 返回的狀態碼和翻譯結果。

    測試步驟：
    1. 發送包含 source_language 和 target_language 的請求到 /zero-shot-query API。
    2. 驗證返回的狀態碼是否為 200。
    3. 驗證返回的翻譯文本是否與預期的文本相匹配。

    參數：
    - source_language: 請求的源語言代碼。
    - target_language: 請求的目標語言代碼。
    - expected_target_sentence: 預期的翻譯後的文本。
    """
    request_payload = {
        "query": "這是一個測試句子",
        "source_language": source_language,
        "target_language": target_language
    }
    response = client.post("/api/translation/zero-shot-query", json=request_payload)
    assert response.status_code == 200, "應返回 200 狀態碼"
    response_json = response.json()
    assert response_json["source_sentence"] == "這是一個測試句子"
    assert response_json["source_language"] == source_language
    assert response_json["target_language"] == target_language

@pytest.mark.parametrize("source_language, target_language, expected_target_sentence", [
    # 測試簡體中文與其他語言之間的翻譯
    ("vi", "zh", "翻譯後的文本"),  # 越南文 -> 簡體中文
])
def test_zero_shot_query_target_languague_is_zh_issue(source_language, target_language, expected_target_sentence):
    """
    """
    request_payload = {
        "query": "這是一個測試句子",
        "source_language": source_language,
        "target_language": target_language
    }
    response = client.post("/api/translation/zero-shot-query", json=request_payload)
    assert response.status_code == 200, "應返回 200 狀態碼"
    response_json = response.json()
    assert response_json["source_sentence"] == "這是一個測試句子"
    assert response_json["source_language"] == source_language
    assert response_json["target_language"] == target_language+"_Hant"

@pytest.mark.parametrize("source_language, target_language, expected_target_sentence", [
    # 測試簡體中文與其他語言之間的翻譯
    ("zh", "vi", "翻譯後的文本"),  # 簡體中文 -> 越南文
])
def test_zero_shot_query_source_languague_is_zh_issue(source_language, target_language, expected_target_sentence):
    """
    """
    request_payload = {
        "query": "這是一個測試句子",
        "source_language": source_language,
        "target_language": target_language
    }
    response = client.post("/api/translation/zero-shot-query", json=request_payload)
    assert response.status_code == 200, "應返回 200 狀態碼"
    response_json = response.json()
    assert response_json["source_sentence"] == "這是一個測試句子"
    assert response_json["source_language"] == source_language+"_Hant"
    assert response_json["target_language"] == target_language

@pytest.mark.parametrize("source_language, target_language, expected_target_sentence", [
    # 測試從繁體中文翻譯到其他語言
    ("zh_Hant", "en", "翻譯後的文本"),  # 繁體中文 -> 英文
    ("zh_Hant", "ja", "翻譯後的文本"),  # 繁體中文 -> 日文
    ("zh_Hant", "ko", "翻譯後的文本"),  # 繁體中文 -> 韓文
    ("zh_Hant", "id", "翻譯後的文本"),  # 繁體中文 -> 印尼文
    ("zh_Hant", "vi", "翻譯後的文本"),  # 繁體中文 -> 越南文
    ("zh_Hant", "th", "翻譯後的文本"),  # 繁體中文 -> 泰文

    # 測試從其他語言翻譯到繁體中文
    ("en", "zh_Hant", "翻譯後的文本"),  # 英文 -> 繁體中文
    ("ja", "zh_Hant", "翻譯後的文本"),  # 日文 -> 繁體中文
    ("ko", "zh_Hant", "翻譯後的文本"),  # 韓文 -> 繁體中文
    ("id", "zh_Hant", "翻譯後的文本"),  # 印尼文 -> 繁體中文
    ("vi", "zh_Hant", "翻譯後的文本"),  # 越南文 -> 繁體中文
    ("th", "zh_Hant", "翻譯後的文本"),  # 泰文 -> 繁體中文
])
def test_zero_shot_query_source_sentence_is_not_equal_target_sentence(source_language, target_language, expected_target_sentence):
    """
    """
    request_payload = {
        "query": "這是一個測試句子",
        "source_language": source_language,
        "target_language": target_language
    }
    response = client.post("/api/translation/zero-shot-query", json=request_payload)
    assert response.status_code == 200, "應返回 200 狀態碼"
    response_json = response.json()
    assert response_json["source_language"] != response_json["target_language"]


@pytest.mark.parametrize("source_language, target_language, expected_target_sentence, strategy_name", [
    # 測試從繁體中文翻譯到其他語言
    ("zh_Hant", "en", "翻譯後的文本", "google"),  # 繁體中文 -> 英文
    ("zh_Hant", "ja", "翻譯後的文本", "google"),  # 繁體中文 -> 日文
    ("zh_Hant", "ko", "翻譯後的文本", "google"),  # 繁體中文 -> 韓文
    ("zh_Hant", "id", "翻譯後的文本", "google"),  # 繁體中文 -> 印尼文
    ("zh_Hant", "vi", "翻譯後的文本", "google"),  # 繁體中文 -> 越南文
    ("zh_Hant", "th", "翻譯後的文本", "google"),  # 繁體中文 -> 泰文

    # 測試從其他語言翻譯到繁體中文
    ("en", "zh_Hant", "翻譯後的文本", "google"),  # 英文 -> 繁體中文
    ("ja", "zh_Hant", "翻譯後的文本", "google"),  # 日文 -> 繁體中文
    ("ko", "zh_Hant", "翻譯後的文本", "google"),  # 韓文 -> 繁體中文
    ("id", "zh_Hant", "翻譯後的文本", "google"),  # 印尼文 -> 繁體中文
    ("vi", "zh_Hant", "翻譯後的文本", "google"),  # 越南文 -> 繁體中文
    ("th", "zh_Hant", "翻譯後的文本", "google"),  # 泰文 -> 繁體中文
])
def test_zero_shot_query_source_sentence_is_not_equal_target_sentence_diff_strategy(source_language, target_language, expected_target_sentence, strategy_name):
    """
    """
    request_payload = {
        "query": "這是一個測試句子",
        "source_language": source_language,
        "target_language": target_language
    }
    params = {"strategy_name": strategy_name}
    response = client.post("/api/translation/zero-shot-query", params=params, json=request_payload)
    assert response.status_code == 200, "應返回 200 狀態碼"
    response_json = response.json()
    assert response_json["source_language"] != response_json["target_language"]